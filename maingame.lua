-- 
-- Abstract: Tilt Monster sample project 
-- Designed and created by Jonathan and Biffy Beebe of Beebe Games exclusively for Ansca, Inc.
-- http://jonbeebe.net/
-- 
-- Version: 2.0.1
-- 
-- Sample code is MIT licensed, see http://developer.anscamobile.com/code/license
-- Copyright (C) 2010 ANSCA Inc. All Rights Reserved.


module(..., package.seeall)

--************************************************************************************
--************************************************************************************

-- Main function - MUST return a display.newGroup()
function new()
	local gameGroup = display.newGroup()
	
	system.setAccelerometerInterval( 73.0 )	-- default: 75.0 (30fps) or 62.0 for 60 fps
	
	-- **************************************************************************************
	-- **************************************************************************************
	-- **************************************************************************************
	-- **************************************************************************************
	
	-- SCORE DISPLAY MODULE
	
	-- **************************************************************************************
	
	
	-- Init images. This creates a map of characters to the names of their corresmilking images.
	 local numbers = { 
		[string.byte("0")] = "0.png",
		[string.byte("1")] = "1.png",
		[string.byte("2")] = "2.png",
		[string.byte("3")] = "3.png",
		[string.byte("4")] = "4.png",
		[string.byte("5")] = "5.png",
		[string.byte("6")] = "6.png",
		[string.byte("7")] = "7.png",
		[string.byte("8")] = "8.png",
		[string.byte("9")] = "9.png",
		[string.byte(" ")] = "space.png",
	}
	
	-- score components
	local theScoreGroup = display.newGroup()
	local theBackground = display.newImageRect( "score-bg.png", 160, 42 )
	theBackground.x = 78; theBackground.y = 20
	local theBackgroundBorder = 10
	
	theBackground.isVisible = true
	
	theScoreGroup:insert( theBackground )
	
	local numbersGroup = display.newGroup()
	theScoreGroup:insert( numbersGroup )
	
	-- the current score
	local theScore = 0
	
	-- the location of the score image
	
	-- initialize the score
	-- 		params.x <= X location of the score
	-- 		params.y <= Y location of the score
	function init( params )
		theScoreGroup.x = 100

		if display.pixelHeight == 1136 then
			theScoreGroup.x = 160
		end

		theScoreGroup.y = -8
		setScore( 0 )
	end
	
	-- retrieve score panel info
	--		result.x <= current panel x
	--		result.y <= current panel y
	--		result.xmax <= current panel x max
	--		result.ymax <= current panel y max
	--		result.stageWidth <= panel width
	--		result.stageHeight <= panel height
	--		result.score <= current score
	function getInfo()
		return {
			x = theScoreGroup.x,
			y = theScoreGroup.y,
			xmax = theScoreGroup.x + theScoreGroup.contentWidth,
			ymax = theScoreGroup.y + theScoreGroup.contentHeight,
			stageWidth = theScoreGroup.contentWidth,
			stageHeight = theScoreGroup.contentHeight,
			score = theScore
		}
	end
	
	-- update display of the current score.
	-- this is called by setScore, so normally this should not be called
	function update()
		-- remove old numerals
		theScoreGroup:remove(2)
	
		local numbersGroup = display.newGroup()
		theScoreGroup:insert( numbersGroup )
	
		-- go through the score, right to left
		local scoreStr = tostring( theScore )
	
		local scoreLen = string.len( scoreStr )
		local i = scoreLen	
	
		-- starting location is on the right. notice the digits will be centered on the background
		local x = theScoreGroup.contentWidth - theBackgroundBorder
		local y = theScoreGroup.contentHeight / 2
		
		while i > 0 do
			-- fetch the digit
			local c = string.byte( scoreStr, i )
			local digitPath = numbers[c]
			--local characterImage = display.newImage( digitPath )
			local characterImage = display.newImageRect( digitPath, 16, 19 )
	
			-- put it in the score group
			numbersGroup:insert( characterImage )
			
			-- place the digit
			characterImage.x = x - characterImage.width / 2
			characterImage.y = y
			x = x - characterImage.width
	
			-- 
			i = i - 1
		end
	end
	
	-- get current score
	function getScore()
		return theScore
	end
	
	-- set score to value
	--	score <= score value
	function setScore( score )
		theScore = score
		
		update()
	end
	
	-- **************************************************************************************
	-- END SCORE MODULE
	-- **************************************************************************************
	-- **************************************************************************************
	-- **************************************************************************************
	-- **************************************************************************************
	
	-- REQUIRED EXTERNAL LIBRARIES
	math.randomseed(os.time())	--> make random more random
	local movieclip = require ("movieclip")	
	local mRandom = math.random
	local physics = require "physics"
	local ui = require("ui")
	local analytics = require "analytics"
	--local facebook = require "facebook"
	local json = require("json")

	local application_key = "RX3SN7CJFDBPMXVKGDF6"
	analytics.init( application_key )
	
	local function printTable( t, label, level )
		if label then print( label ) end
		level = level or 1
	
		if t then
			for k,v in pairs( t ) do
				local prefix = ""
				for i=1,level do
					prefix = prefix .. "\t"
				end
	
				print( prefix .. "[" .. tostring(k) .. "] = " .. tostring(v) )
				if type( v ) == "table" then
					print( prefix .. "{" )
					printTable( v, nil, level + 1 )
					print( prefix .. "}" )
				end
			end
		end
	end
	
	---- Check if running on device or simulator ----
	local onDevice = false
	
	if system.getInfo( "environment" ) == "device" then
		onDevice = true
	else
		onDevice = false
	end
	
	-- GAME SETTINGS
	local deviceiPad = false
	local gameIsActive = false
	local menuIsActive = false
	local newHighScore = false
	local gameLives = 5
	local grassCombo = 0
	local grassCount = 0
	local highestCombo = 0
	local treeCycle = 1
	local pickupCycle = 1
	local starCycle = 1
	local BadFenceCycle = 2
	local milkCycle = 2
	local checkPointCycle = 1
	local cpCount = 0
	local starCount = 0
	local orientationDirection = "landscapeRight"
	
	local gameSettings = {
		shouldOptimize = false,
		gameTheme = "classic",		--> classic, spook, disco, underwater
		gameChar = "ms. d",				--> d, ms. d
		tiltSpeed = "3",
		defaultMoveSpeed = 12,		--> set to 3.7 for 60 fps; 7.7 for 30 fps
		gameMoveSpeed = 12,
		soundsOn = true,
		musicOn = false,
		bestScore = 0,
		lifegrasss = 0,
		highCombo = 0,
		oldUnlocked = 0,
		difficulty = "easy"			--> "easy", "medium", "hard"
	}
	
	-- FOR OPENFEINT ACHIEVEMENTS
	local ofAch = {
		score_10k = false,
		score_20k = false,
		score_50k = false,
		score_70k = false,
		combo_5 = false,
		combo_15 = false,
		combo_25 = false,
		combo_30 = false,
		combo_40 = false,
		combo_50 = false,
		combo_60 = false,
		grasss_1k = false,
		grasss_5k = false,
		grasss_10k = false,
		grasss_15k = false,
		grasss_25k = false,
		grasss_35k = false,
		grass_king = false,
		zero_hero = false,
		under_achiever = false,
		grass_hater = false,
		slacker = false,
		super_slacker = false,
		heart_saver = false,
		stingy_heart = false,
		life_preserver = false,
		fifteen_stars = false,
		star_struck = false
	}
	
	-- RANDOM TABLES
	local randomBunnyLocations = {}
	local bunnyIndice = 1; local maxBunnyIndice = 20
	local randomgrassLocations = {}
	local grassIndice = 1; local maxgrassIndice = 15
	
	local random1to4Table = {}
	local oneFourIndice = 1; local maxOneFourIndice = 20
	
	-- OBJECTS
	local playerObject
	local electroBubble
	local grassObject1
	local grassObject2
	local treeObjects = { }
	local starObject; local starSpawnRate = 1
	--local pickupObject; local pickupSpawnRate = 6
	local checkPointObject; local checkPointSpawnRate = 8
	local grassBlade1; local grassBlade2; local grassBlade3
	
	-- ENEMY OBJECTS
	local flyingBunny1
	local flyingBunny2
	-- local flyingBunny3
	-- local bombObject1
	local BadFence; local BadFenceSpawnRate = 1
	local milkObject; local milkSpawnRate = 3
	
	-- HUD OBJECTS
	local comboIcon
	local comboText
	local comboBackground
	local notificationText
	local notificationBanner
	-- local heartBackground
	local heartLeft
	local heartLeft2
	local heartLeft3
	local heartLeft4
	local heartRight
	local damageRect
	local pickupRect
	local pauseOverlay
	local bestScoreText
	local grassScoreText
	local gameOverScoreText
	local gameOverScoreBanner
	local gameOverShade
	local highScoreMarker
	local ofBtn
	local tryAgainBtn
	local menuBtn
	local calciumAward
	local quickStartBanner	--> quick start menu image
	local playNowBtn	--> quick start menu button
	local helpBtn		--> quick start menu button
	local creditsBtn 	--> quick start menu button
	local floatingText
	local floatingTextStar
	
	-- COLLISION FILTERS
	local playerFilter = { categoryBits = 2, maskBits = 6 }		--> Mask 6 collide with 4 & 2
	local itemMonsterFilter = { categoryBits = 4, maskBits = 3 }	--> Mask 3 collide with 2 & 1
	
	-- SOUNDS
	local pickupSound = audio.loadSound( "pickup.caf" )
	local grassSound = audio.loadSound( "chew.wav" )
	local tapSound = audio.loadSound( "tapsound.caf" )
	local hurtSound = audio.loadSound( "cow-moo.mp3" )
	-- local bombSound = audio.loadSound( "bomb.caf" )
	local gameOverSound = audio.loadSound( "cow-moo.mp3" )
	local checkPointSound = audio.loadSound( "checkpoint.mp3" )
	
	local musicChan
	local gameMusic1 = audio.loadStream( "intro-moo.mp3" )		--> menu music
	local gameMusic2 = audio.loadStream( "intro-moo.mp3" )		--> cool halloween music
	local gameMusic3 = audio.loadStream( "intro-moo.mp3" )		--> doodle dash music
	
	local runningSound = audio.loadStream( "footsteps.mp3" )	--> running (sound effect, play as music)
	
	-- set default master volume
	audio.setVolume( 1.0 )
	
	--***************************************************
	
	-- unloadSoundsAndMusic()
	
	--***************************************************
	
	local unloadSoundsAndMusic = function()
		
		audio.stop()
		
		audio.dispose( pickupSound )
			pickupSound = nil
		audio.dispose( grassSound )
			grassSound = nil
		audio.dispose( tapSound )
			tapSound = nil
		audio.dispose( hurtSound )
			hurtSound = nil
		-- audio.dispose( bombSound )
		-- 	bombSound = nil
		audio.dispose( gameOverSound )
			gameOverSound = nil
		audio.dispose( checkPointSound )
			checkPointSound = nil
		
		audio.dispose( gameMusic1 )
			gameMusic1 = nil
		audio.dispose( gameMusic2 )
			gameMusic2 = nil
		audio.dispose( gameMusic3 )
			gameMusic3 = nil
		audio.dispose( runningSound )
			runningSound = nil
	end
	
	--***************************************************

	-- saveValue() --> used for saving high score, etc.
	
	--***************************************************
	local saveValue = function( strFilename, strValue )
		-- will save specified value to specified file
		local theFile = strFilename
		local theValue = strValue
		
		local path = system.pathForFile( theFile, system.DocumentsDirectory )
		
		-- io.open opens a file at path. returns nil if no file found
		local file = io.open( path, "w+" )
		if file then
		   -- write game score to the text file
		   file:write( theValue )
		   io.close( file )
		end
	end
	
	--***************************************************

	-- loadValue() --> load saved value from file (returns loaded value as string)
	
	--***************************************************
	local loadValue = function( strFilename )
		-- will load specified file, or create new file if it doesn't exist
		
		local theFile = strFilename
		
		local path = system.pathForFile( theFile, system.DocumentsDirectory )
		
		-- io.open opens a file at path. returns nil if no file found
		local file = io.open( path, "r" )
		if file then
		   -- read all contents of file into a string
		   local contents = file:read( "*a" )
		   io.close( file )
		   return contents
		else
		   -- create file b/c it doesn't exist yet
		   file = io.open( path, "w" )
		   file:write( "0" )
		   io.close( file )
		   return "0"
		end
	end
	
	--***************************************************

	-- comma_value() --> place comma in thousands
	
	--***************************************************
	
	local comma_value = function(amount)
	  local formatted = amount
	  while true do  
		formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
		if (k==0) then
		  break
		end
	  end
	  return formatted
	end
	
	-- ************************************************************** --
	
	--	flashAnimation() -- flash a colored rectangle onscreen
	
	-- ************************************************************** --
	
	local flashAnimation = function( flashType )
		
		if flashType == "damage" then
			
			damageRect.alpha = 1.0
			damageRect.isVisible = true
			
			local secondPickupRound = function()
				
				damageRect.alpha = 0.65
				damageRect.isVisible = true
				
				local hidedamageRect = function()
					damageRect.isVisible = false
				end
				
				transition.to( damageRect, { alpha=0, time=275, onComplete=hidedamageRect } )
			end
			
			transition.to( damageRect, { alpha=0, time=200, onComplete=secondPickupRound } )
		elseif flashType == "pickup" then
			
			pickupRect.alpha = 0.65
			pickupRect.isVisible = true
			
			local secondPickupRound = function()
				
				pickupRect.alpha = 0.65
				pickupRect.isVisible = true
				
				local hidePickupRect = function()
					pickupRect.isVisible = false
				end
				
				transition.to( pickupRect, { alpha=0, time=275, onComplete=hidePickupRect } )
			end
			
			transition.to( pickupRect, { alpha=0, time=200, onComplete=secondPickupRound } )
		end
		
	end
	
	local flashAnimationPickup = function( flashType )
		
		if flashType == "damage" then
			
			damageRect.alpha = 1
			damageRect.isVisible = true
			
			local hideDamageRect = function()
				damageRect.isVisible = false
			end
			
			transition.to( damageRect, { alpha=0, time=275, onComplete=hideDamageRect } )
		elseif flashType == "pickup" then
			
			pickupRect.alpha = 0.65
			pickupRect.isVisible = true
			
			local secondPickupRound = function()
				
				pickupRect.alpha = 0.65
				pickupRect.isVisible = true
				
				local hidePickupRect = function()
					pickupRect.isVisible = false
				end
				
				transition.to( pickupRect, { alpha=0, time=275, onComplete=hidePickupRect } )
			end
			
			transition.to( pickupRect, { alpha=0, time=200, onComplete=secondPickupRound } )
		end
	
	end
	-- ************************************************************** --
	
	--	onTilt() -- Accelerometer Code for Player Movement
	
	-- ************************************************************** --
	
	local onTilt = function( event )
		if gameIsActive then
			
			if playerObject.x <= 10 then
				playerObject.x = 10
			elseif playerObject.x >= 470 then
				playerObject.x = 470
			end

			if display.pixelHeight == 1136  then
				if playerObject.x <= 30 then
					playerObject.x = 30
				elseif playerObject.x >= 558 then
					playerObject.x = 558
				end
			end
			
			playerObject.x = playerObject.x - (playerObject.speed * event.yGravity)
		end
	end
	
	-- ************************************************************** --
	
	--	dropNotification() -- drop down notification message from top
	
	-- ************************************************************** --
	
	local dropNotification = function( strMessage )
		
		local theMessage = strMessage
		
		notificationText.text = theMessage
		notificationText:setReferencePoint( display.CenterLeftReferencePoint )
		notificationText.x = 260

		if display.pixelHeight == 1136 then
			notificationText.x = 345
		end

		notificationText.isVisible = true
		
		notificationBanner:setReferencePoint( display.TopLeftReferencePoint )
		notificationBanner.x = 0
		notificationBanner.isVisible = true

		if display.pixelHeight == 1136 then
			notificationBanner.x = 0
		end
		
		local startHideTimer = function()
			local hideNotification = function()
				local hideBannerAndText = function()
					notificationText.isVisible = false
					notificationBanner.isVisible = false
				end
				
				transition.to( notificationText, { time=1000, y=-18, transition=easing.inOutExpo, onComplete=hideBannerAndText })
				transition.to( notificationBanner, { time=1000, y=-32, transition=easing.inOutExpo })
			end
			
			local hideTimer = timer.performWithDelay( 2000, hideNotification, 1 )
		end
		
		transition.to( notificationBanner, { time=1000, y=0, transition=easing.inOutExpo, onComplete=startHideTimer })
		transition.to( notificationText, { time=1000, y=16, transition=easing.inOutExpo })
		
	end
	
	-- ************************************************************** --
	
	--	touchPause() -- pause/unpause the game
	
	-- ************************************************************** --
	
	local touchPause = function( event )

		analytics.logEvent("Game Paused")
		
		if event.phase == "began" then
			if event.x > 50 and event.x < 430 and event.y > 50 and event.y < 270 then
				if gameIsActive then
					-- Pause the game
					gameIsActive = false
					system.setIdleTimer( true ) -- turn on device sleeping
					Runtime:removeEventListener( "accelerometer", onTilt )
					
					-- START PAUSE SCREEN
					
					-- Pause Overlay
					pauseOverlay = display.newImageRect( "pauseoverlay.png", 440, 277 )
					pauseOverlay.x = 240; pauseOverlay.y = 160
					pauseOverlay.isVisible = true

					if display.pixelHeight == 1136 then
						pauseOverlay.x = 284
					end

					
					gameGroup:insert( pauseOverlay )
					
					-- END PAUSE SCREEN
					
					physics.pause()
					
					local musicOn = gameSettings[ "musicOn" ]
					local soundsOn = gameSettings[ "soundsOn" ]
					
					if musicOn then
						audio.setVolume( 0, { channel=musicChan } )	--> OpenAL
					end
					
					if soundsOn then
						audio.setVolume( 0 )
					end
				else
					-- Unpause the game
					system.setIdleTimer( false ) -- turn off device sleeping
					gameIsActive = true
					Runtime:removeEventListener( "accelerometer", onTilt )
					Runtime:addEventListener( "accelerometer", onTilt )
					
					if pauseOverlay then
						pauseOverlay.isVisible = false;
						display.remove( pauseOverlay )
						pauseOverlay = nil
					end
					
					physics.start( true )
					
					local musicOn = gameSettings[ "musicOn" ]
					local soundsOn = gameSettings[ "soundsOn" ]
					
					if musicOn then
						audio.setVolume( 0.5, { channel=musicChan } )		--> OpenAL
					end
					
					if soundsOn then
						audio.setVolume( 1.0 )
					end
				end
			end
		end
	end
	
	-- ************************************************************** --
	
	--	onSystem() -- listener for system events
	
	-- ************************************************************** --
	
	local onSystem = function( event )
		if event.type == "applicationSuspend" then
			if gameIsActive then
				-- Pause the game
				gameIsActive = false
				system.setIdleTimer( true ) -- turn on device sleeping
				Runtime:removeEventListener( "accelerometer", onTilt )
				
				-- START PAUSE SCREEN
					
				-- Pause Overlay
				pauseOverlay = display.newImageRect( "pauseoverlay.png", 440, 277 )
				pauseOverlay.x = 240; pauseOverlay.y = 160
				pauseOverlay.isVisible = true


				if display.pixelHeight == 1136 then
					pauseOverlay.x = 284
				end
				
				gameGroup:insert( pauseOverlay )
				
				-- END PAUSE SCREEN
				
				physics.pause()
				
				local musicOn = gameSettings[ "musicOn" ]
				
				if musicOn then
					audio.setVolume( 0, { channel=musicChan } )	--> OpenAL
				end
			end
		elseif event.type == "applicationExit" then
			-- save some data
			
			if onDevice == true then
				-- and then quit the game
				os.exit()
			end
		end
	end
	
	-- ************************************************************** --
	
	-- recycleRound() -- Start a new round (not the first!)
	
	-- ************************************************************** --
	
	local hideGameOverScreenObjects = function()
		-- Fade out the game over shade
		if gameOverShade then
			transition.to( gameOverShade, { time=500, alpha=0 } )
		end
		
		local hideGameOverScoreBanner = function()
			gameOverScoreBanner.isVisible = false
			grassScoreBg.isVisible = false
		end
		
		if gameOverScoreBanner then
			transition.to( gameOverScoreBanner, { x=720, time=1000, transition=easing.inOutExpo, onComplete=hideGameOverScoreBanner } )
		end
		
		local hideQuickStartBanner = function()
			quickStartBanner.isVisible = false
			--quickStartBanner:removeSelf()
			display.remove( quickStartBanner )
			quickStartBanner = nil
		end
		
		if quickStartBanner then
			transition.to( quickStartBanner, { x=720, time=1000, transition=easing.inOutExpo, onComplete=hideQuickStartBanner } )
		end
		
		if grassScoreBg then
			transition.to( grassScoreBg, { y=-52, time=1000, transition=easing.inOutExpo } )
		end
		
		if grassScoreText then
			grassScoreText.isVisible = false
		end
		
		if gameOverScoreText then
			gameOverScoreText.isVisible = false
		end
		
		if highScoreMarker then
			highScoreMarker.isVisible = false
		end
		
		if tryAgainBtn then
			local dropTryAgainButton = function()
				local hideTryAgain = function() tryAgainBtn.isVisible = false; tryAgainBtn.isActive = true; end
				transition.to( tryAgainBtn, { y=600, time=1000, transition=easing.inOutExpo, onComplete=hideTryAgain } )
			end
			local newTryAgainY = tryAgainBtn.y - 10
			transition.to( tryAgainBtn, { y=newTryAgainY, time=200, onComplete=dropTryAgainButton } )
		end
		
		if playNowBtn then
			local dropPlayNowButton = function()
				local hidePlayNow = function() playNowBtn.isVisible = false; display.remove( playNowBtn ); playNowBtn = nil; end
				transition.to( playNowBtn, { y=600, time=1000, transition=easing.inOutExpo, onComplete=hidePlayNow } )

				local hideDairyLogo = function() 
					dairyLogo.isVisible = false; display.remove( dairyLogo ); dairyLogo = nil;
				end
				transition.to ( dairyLogo, { y=-100, time = 1000, transition=easing.inOutExpo, onComplete=hideDairyLogo } )

				local hideCreditsBtn = function() 
					creditsBtn.isVisible = false; display.remove( creditsBtn ); creditsBtn = nil;
				end
				transition.to ( creditsBtn, { y=400, time = 100, transition=easing.inOutExpo, onComplete=hideCreditsBtn } )

				local hideHomeBg = function() 
					homeBackGround.isVisible = false; display.remove( homeBackGround ); homeBackGround = nil;
				end
				transition.to ( homeBackGround, { x=1000, time = 1000, transition=easing.inOutExpo, onComplete=homeBackGround } )
			end
			
			local newPlayNowY = playNowBtn.y - 10
			transition.to( playNowBtn, { y=newPlayNowY, time=200, onComplete=dropPlayNowButton } )
		end
		
		if ofBtn then
			local hideOfBtn = function() ofBtn.isVisible = false; end
			transition.to( ofBtn, { y=625, time=1000, transition=easing.inOutExpo, onComplete=hideOfBtn } )
		end
		
		-- if fbBtn then
		-- 	local hideFbBtn = function() fbBtn.isVisible = false; end
		-- 	transition.to( fbBtn, { y=625, time=1000, transition=easing.inOutExpo, onComplete=hideFbBtn } )
		-- end
		
		if helpBtn then
			local hideHelpBtn = function() helpBtn.isVisible = false; display.remove( helpBtn ); helpBtn = nil; end
			transition.to( helpBtn, { y=450, time=1000, transition=easing.inOutExpo, onComplete=hideHelpBtn } )

			local hideDairyLogo = function() 
				dairyLogo.isVisible = false; display.remove( dairyLogo ); dairyLogo = nil;
			end
			transition.to ( dairyLogo, { y=-100, time = 1000, transition=easing.inOutExpo, onComplete=hideDairyLogo } )

		end
		
		--[[
		if tournamentBtn then
			local hideTournamentBtn = function() tournamentBtn.isVisible = false; tournamentBtn:removeSelf(); tournamentBtn = nil; end
			transition.to( tournamentBtn, { y=450, time=1000, transition=easing.inOutExpo, onComplete=hideTournamentBtn } )
		end
		]]--
		
		if menuBtn then
			local hideMenuBtn = function() menuBtn.isVisible = false; end
			transition.to( menuBtn, { y=450, time=1000, onComplete=hideMenuBtn } )
		end
		
		
		-- Reshow on-screen hud elements
		theScoreGroup.isVisible = true
		-- heartBackground.isVisible = false
		heartLeft.isVisible = true
		heartLeft2.isVisible = true
		heartLeft3.isVisible = true
		heartLeft4.isVisible = true
		heartRight.isVisible = true
		bestScoreText.text = comma_value(gameSettings[ "bestScore" ])
		--bestScoreText:setReferencePoint( display.CenterLeftReferencePoint )
		--bestScoreText.x = 25
		bestScoreText.xScale = 0.5; bestScoreText.yScale = 0.5
		bestScoreText.x = (bestScoreText.contentWidth / 2) + 100; bestScoreText.y = 12

		if display.pixelHeight == 1136 then
			bestScoreText.x = (bestScoreText.contentWidth / 2) +125; bestScoreText.y = 12
		end
	end
	
	local recycleRound = function()
		
		--First, make sure accelerometer events are turned off (so they don't get turned on double time)
		Runtime:removeEventListener( "accelerometer", onTilt )
		
		-- First, reset the score and game lives
		--frameCounter = 1
		setScore( 0 )
		grassCombo = 0
		grassCount = 0
		highestCombo = 0
		treeCycle = 1
		pickupCycle = 1
		checkPointCycle = 1
		cpCount = 0
		starCount = 0
		starCycle = 1
		BadFenceCycle = 2
		milkCycle = 2
		newHighScore = false
		gameLives = 5; heartLeft.alpha = 1; heartLeft2.alpha = 1; heartLeft3.alpha = 1; heartLeft4.alpha = 1
		gameSettings["gameMoveSpeed"] = gameSettings["defaultMoveSpeed"]
		
		-- make sure best score is back to black
		local gameTheme = gameSettings["gameTheme"]
		
		-- if gameTheme == "spook" then
		-- 	bestScoreText:setTextColor( 0, 0, 0, 255 )
		-- else
		-- 	bestScoreText:setTextColor( 225, 0, 0, 255)
		-- end
		
		-- Populate random tables
		for i = 1, maxBunnyIndice do
			randomBunnyLocations[i] = mRandom( 1, 479 )
		end
		
		for i = 1, maxgrassIndice do
			randomgrassLocations[i] = mRandom( 70, 410 )
		end
		
		for i = 1, maxOneFourIndice do
			random1to4Table[i] = mRandom( 1, 4 )
		end
		
		
		-- Reset player settings
		playerObject.x = 240
		if display.pixelHeight == 1136 then
			playerObject.x = 300
		end
		playerObject.y = 125
		playerObject.framePosition = "left"
		playerObject.alpha = 1
		playerObject.isVisible = true
		playerObject.animInterval = 150
		playerObject.isInvisible = false
		playerObject.invisibleCycle = 1
		playerObject.isElectro = false
		playerObject.alpha = 1.0
		playerObject.isBodyActive = true
		
		-- set player speed multiplier based on tilt sensitivity (tiltSpeed)
		local tiltSpeed = gameSettings["tiltSpeed"]
		
		if tiltSpeed == "1" then
			playerObject.speed = 19
			
		elseif tiltSpeed == "2" then
			playerObject.speed = 22
			
		elseif tiltSpeed == "3" then
			playerObject.speed = 25
			
		elseif tiltSpeed == "4" then
			playerObject.speed = 32
			
		elseif tiltSpeed == "5" then
			playerObject.speed = 37
			
		else
			playerObject.speed = 25
		
		end
		
		-- Reset grasss
		grassObject1.x = randomgrassLocations[grassIndice]
		grassIndice = grassIndice + 1
		if grassIndice > maxgrassIndice then
			grassIndice = 1
		end
		grassObject1.y = 420
		grassObject2.x = randomgrassLocations[grassIndice]
		grassIndice = grassIndice + 1
		if grassIndice > maxgrassIndice then
			grassIndice = 1
		end
		grassObject2.y = 620
		
		grassObject1.isVisible = true; grassObject1.alpha = 1.0; grassObject1.isBodyActive = true
		grassObject2.isVisible = true; grassObject2.alpha = 1.0; grassObject2.isBodyActive = true
		
		-- Reset grass
		-- 
		
		-- Reset pickup object
		pickupObject.x = randomgrassLocations[grassIndice];
		grassIndice = grassIndice + 1
		if grassIndice > maxgrassIndice then
			grassIndice = 1
		end
		
		pickupObject.y = 520
		pickupObject.onTheMove = false
		pickupObject.isVisible = false
		pickupObject.isBodyActive = false
		electroBubble.isVisible = false
		
		-- Reset checkpoint object
		checkPointObject.x = 240
		checkPointObject.y = 600
		checkPointObject.onTheMove = false
		checkPointObject.isVisible = false
		checkPointObject.isBodyActive = false
		
		-- Reset star object
		starObject.x = randomgrassLocations[grassIndice];
		grassIndice = grassIndice + 1
		if grassIndice > maxgrassIndice then
			grassIndice = 1
		end
		
		starObject.y = 720
		starObject.onTheMove = false
		starObject.isVisible = false
		starObject.isBodyActive = false
		
		-- Reset bunnies
		flyingBunny1.x = randomBunnyLocations[bunnyIndice]
		bunnyIndice = bunnyIndice + 1
		if bunnyIndice > maxBunnyIndice then
			bunnyIndice = 1
		end
		flyingBunny1.y = 500
		
		flyingBunny2.x = randomBunnyLocations[bunnyIndice]
		bunnyIndice = bunnyIndice + 1
		if bunnyIndice > maxBunnyIndice then
			bunnyIndice = 1
		end
		
		flyingBunny2.y = 800
		
		if gameSettings[ "difficulty" ] ~= "easy" then
			flyingBunny3.x = randomBunnyLocations[bunnyIndice]
			bunnyIndice = bunnyIndice + 1
			if bunnyIndice > maxBunnyIndice then
				bunnyIndice = 1
			end
		
			flyingBunny3.y = 950
		end
		
		flyingBunny1.isVisible = true; flyingBunny1.isBodyActive = true
		flyingBunny2.isVisible = true; flyingBunny2.isBodyActive = true
		flyingBunny1.selfSpeed = 0.5; flyingBunny2.selfSpeed = 0.5
		
		if gameSettings[ "difficulty" ] ~= "easy" then
			flyingBunny3.isVisible = false; flyingBunny3.isBodyActive = false; flyingBunny3.selfSpeed = 0.5
		end
		
		-- Reset bombs
		-- bombObject1.x = randomgrassLocations[grassIndice]
		-- grassIndice = grassIndice + 1;
		-- if grassIndice > maxgrassIndice then
		-- 	grassIndice = 1
		-- end
		-- bombObject1.y = 650
		
		-- bombObject1.isVisible = true; bombObject1.isBodyActive = true;
		
		-- Reset big enemy
		BadFence.x = randomBunnyLocations[bunnyIndice];
		bunnyIndice = bunnyIndice + 1
		if bunnyIndice > maxBunnyIndice then
			bunnyIndice = 1
		end
		
		BadFence.y = 900
		BadFence.onTheMove = false
		BadFence.isVisible = false
		BadFence.isBodyActive = false
		BadFence.isDestroyed = false
		
		-- Reset green milk
		milkObject.x = randomBunnyLocations[bunnyIndice];
		bunnyIndice = bunnyIndice + 1
		if bunnyIndice > maxBunnyIndice then
			bunnyIndice = 1
		end
		
		milkObject.y = 500
		milkObject.onTheMove = false
		milkObject.isVisible = true
		milkObject.isBodyActive = false
		milkObject.isDestroyed = false
		
		
		-- Remove/hide Game Over Screen Objects
		
		hideGameOverScreenObjects()
		
		--collectgarbage( "collect" )
		timer.performWithDelay(1, function() collectgarbage("collect") end)
		
		-- Start the new round
		system.setIdleTimer( false ) -- turn off device sleeping
		
		local startNewRound = function() menuIsActive = false; gameIsActive = true; physics.start(); end
		
		local startGameInOne = timer.performWithDelay( 500, startNewRound, 1 )
		
		Runtime:addEventListener( "accelerometer", onTilt )
		Runtime:addEventListener( "touch", touchPause )
		
		playerObject.isVisible = true
	end
	
	-- ************************************************************** --
	
	-- connectHandler (for facebook posting)
	
	-- ************************************************************** --
	
	local function connectHandler( event )
		local post = "Playing CowQuest -- Getting Addicted!"
		local gameScore = getScore()
		gameScore = comma_value(gameScore)
		
		local session = event.sender
		if ( session:isLoggedIn() ) then
	
			print( "fbStatus " .. session.sessionKey )
			
			local scoreStatusMessage = "just scored a " .. tostring(gameScore) .. " on Tilt Monster... I'm officially addicted."
			
			local attachment = {
				name="Download Tilt Monster To Compete With Me",
				caption="Think you can beat my score of " .. tostring(gameScore) .. "? I dare you to try!",
				href="http://---LINK-TO-YOUR-ITUNES-APP",
				media= { { type="image", src="http://---LINK-TO-YOUR-90x90-ICON", href="---LINK-TO-YOUR-ITUNES-APP" } }
			}
	
			local action_links = {
				{ text="Download Tilt Monster", href="http://beebegamesonline.appspot.com/tiltmonster-itunes.html" }
			}
	
			local response = session:call{
				message = scoreStatusMessage,
				method ="stream.publish",
				attachment = json.encode(attachment),
				action_links = json.encode(action_links),
			}
	
			if "table" == type(response ) then
				-- print contents of response upon failure
				printTable( response, "fbStatus response:", 5 )
			end
			
			local onComplete = function ( event )
				if "clicked" == event.action then
					local i = event.index
					if 1 == i then
						-- Player click 'Ok'; do nothing, just exit the dialog
					end
				end
			end
			
			-- Show alert with two buttons
			local alert = native.showAlert( "Tilt Monster", "Your score has been posted to Facebook.", 
													{ "Ok" }, onComplete )
		end
	end
	
	
	-- **************************************************************
	
	-- drawGameOverScreen()
	
	-- **************************************************************
	
	local drawGameOverScreen = function()
		
		-- Draw game over shade
		if not gameOverShade then
			gameOverShade = display.newImageRect( "gameover.png", 630, 320 )		--> "gameovershade.png", 480, 320
			gameOverShade.x = 315; gameOverShade.y = 160

			

			gameOverShade.isVisible = false; gameOverShade.alpha = 0
			
			gameGroup:insert( gameOverShade )
		end
		
		-- Draw banner that grass score will show up on
		if not grassScoreBg then
			grassScoreBg = display.newImageRect( "grass-score-bg.png", 284, 48 )
			grassScoreBg.x = -52; grassScoreBg.x = 240
			grassScoreBg.isVisible = false
			
			--gameGroup:insert( grassScoreBg )
		end
		
		
		-- Draw banner that final score will show up on
		if not gameOverScoreBanner then
			gameOverScoreBanner = display.newImageRect( "highscorebanner.png", 480, 84 )
			gameOverScoreBanner.x = 720; gameOverScoreBanner.y = 94
			gameOverScoreBanner.isVisible = false
			
			gameGroup:insert( gameOverScoreBanner )
		end
		
		-- Draw high score marker that will show up if player beats their previous high score
		if not highScoreMarker then
			highScoreMarker = display.newImageRect( "highscoremarker.png", 50, 50 )
			highScoreMarker.x = 384; highScoreMarker.y = 88
			highScoreMarker.isVisible = false
		
			gameGroup:insert( highScoreMarker )
		end
		
		-- Draw grass score text with empty value
		if not grassScoreText then
			grassScoreText = display.newText( "High grass Combo: ", 240, -52, "Helvetica-Bold", 30 )
			grassScoreText:setTextColor( 0, 178, 221, 255 )
			grassScoreText.xScale = 0.5; grassScoreText.yScale = 0.5
			grassScoreText.x = 240; grassScoreText.y = -52
			grassScoreText.isVisible = false
			
			gameGroup:insert( grassScoreText )
		end
		
		-- Draw high score text with temporary score
		if not gameOverScoreText then
			gameOverScoreText = display.newText( "1,000", 240, 93, "Helvetica-Bold", 76 )
			gameOverScoreText:setTextColor( 0, 205, 255, 255 )
			gameOverScoreText.xScale = 0.5; gameOverScoreText.yScale = 0.5
			gameOverScoreText.x = 240; gameOverScoreText.y = 93
			gameOverScoreText.isVisible = false
			
			gameGroup:insert( gameOverScoreText )
		end
		
		-- Setup "Try Again" Button
		local touchTryAgainBtn = function( event )
			analytics.logEvent("Try Again Pressed")
			if event.phase == "release" and tryAgainBtn.isActive == true then
				
				tryAgainBtn.isActive = false
				
				-- Play Sound
				local soundsOn = gameSettings[ "soundsOn" ]
				
				if soundsOn == true then
					local freeChan = audio.findFreeChannel()
					audio.play( tapSound, { channel=freeChan } )
					
					local freeChan2 = audio.findFreeChannel()
					audio.play( runningSound, { loops=-1, channel=freeChan2 } )
				end
				
				recycleRound()
			end
		end
		
		if not tryAgainBtn then
			tryAgainBtn = ui.newButton{
				defaultSrc = "tryagain.png",
				defaultX = 178,
				defaultY = 39,
				overSrc = "tryagain-o.png",
				overX = 178,
				overY = 39,
				onEvent = touchTryAgainBtn,
				id = "tryAgainButton",
				text = "",
				font = "Helvetica",
				textColor = { 255, 255, 255, 255 },
				size = 16,
				emboss = false
			}
			
			tryAgainBtn.xOrigin = 400; tryAgainBtn.yOrigin = 600
			tryAgainBtn.isVisible = false
			
			gameGroup:insert( tryAgainBtn )
		end
		
		if ofBtn then gameGroup:insert( ofBtn ); end
		
		
		
		-- Setup "Menu" Button
		local touchMenuBtn = function( event )
			analytics.logEvent("Menu Button Pressed")

			if event.phase == "release" and menuBtn.isActive == true then
				menuBtn.isActive = false
				
				-- Play Sound
				local soundsOn = gameSettings[ "soundsOn" ]
				local musicOn = gameSettings[ "musicOn" ]
				
				if soundsOn == true then
					local freeChan = audio.findFreeChannel()
					audio.play( tapSound, { channel=freeChan } )
				end
				
				if musicOn == true then
				
					if gameSettings[ "gameTheme" ] == "classic" then
						audio.stop( gameMusic3 )
					elseif gameSettings[ "gameTheme" ] == "spook" then
						audio.stop( gameMusic2 )
					end
				
				end
				
				--main menu call
				director:changeScene( "gotomainmenu" )
			end
		end
		
		if not menuBtn then
			menuBtn = ui.newButton{
				defaultSrc = "menu-yellow.png",
				defaultX = 178,
				defaultY = 39,
				onEvent = touchMenuBtn,
				id = "menuButton"
			}
			

			menuBtn.yOrigin = 450
			menuBtn.isVisible = false
			menuBtn.x=200
			
			gameGroup:insert( menuBtn )
		end
	end
	
	-- END DRAW GAME OVER SCREEN
	
	
	-- ************************************************************** --
	
	-- callGameOver() -- Display the game over display overlay
	
	-- ************************************************************** --
	
	local callGameOver = function()
		-- Pause all game movement
		if gameIsActive == true then gameIsActive = false; end
		
		physics.pause()
		Runtime:removeEventListener( "accelerometer", onTilt )
		Runtime:removeEventListener( "touch", touchPause )
		
		-- Play Game Over Sound
		local soundsOn = gameSettings[ "soundsOn" ]
		
		if soundsOn then
			-- stop running sound
			audio.stop()
			
			local freeChan = audio.findFreeChannel()
			audio.play( gameOverSound, { channel=freeChan } )
		end
		
		system.setIdleTimer( true ) -- turn on device sleeping
		
		
		-- Hide some of the on-screen elements
		theScoreGroup.isVisible = false
		-- heartBackground.isVisible = false
		heartLeft.isVisible = false
		heartLeft2.isVisible = false
		heartLeft3.isVisible = false
		heartLeft4.isVisible = false
		heartRight.isVisible = false
		comboText.isVisible = false
		comboIcon.isVisible = false
		comboBackground.isVisible = false
		
		-- Create Game Over Screen
		drawGameOverScreen()
		
		local showGameOverScreen = function()
			
			-- Store current score as variable
			local scoreNumber = getScore()
			scoreNumber = comma_value(scoreNumber)
			
			-- Check to see if there is a new high score, if so, save it
			local gameScore = tonumber(getScore())
			local bestScore = tonumber(gameSettings[ "bestScore" ])
			
			if gameScore > bestScore then
				gameSettings[ "bestScore" ] = tostring(gameScore)
				bestScore = gameScore
				
				if gameSettings[ "difficulty" ] ~= "easy" then
					saveValue( "TiMQGcpCZv.data", tostring(gameScore) )
				else
					saveValue( "TpjixLATIZ.data", tostring(gameScore) )
				end
				
				-- Show high score marker
				highScoreMarker.alpha = 100
				highScoreMarker.isVisible = true
				transition.to( highScoreMarker, { time=2000, alpha=1, transition=easing.inOutExpo } )

				if display.pixelHeight == 1136 then
					highScoreMarker.x = 440
				end

			end
			
			
			-- Update lifetime grasss file
			local lifegrasssCount = tonumber(gameSettings[ "lifegrasss" ])
			lifegrasssCount = lifegrasssCount + grassCount
			gameSettings[ "lifegrasss" ] = tostring(lifegrasssCount)
			
			if gameSettings[ "difficulty" ] ~= "easy" then
				saveValue( "SadzCtDWmK.data", tostring(lifegrasssCount) )
			else
				saveValue( "sOfvDxAlkH.data", tostring(lifegrasssCount) )
			end
			
			-- Update highest combo file
			local bestCombo = tonumber(gameSettings[ "highCombo" ])
			
			if highestCombo > bestCombo then
				gameSettings[ "highCombo" ] = tostring(highestCombo)
				
				if gameSettings[ "difficulty" ] ~= "easy" then
					saveValue( "UVIMSPUuCb.data", tostring(highestCombo) )
				else
					saveValue( "wnpzK3g55u.data", tostring(highestCombo) )
				end
			end
			
			bestCombo = tonumber(gameSettings[ "highCombo" ])
		
			
		
			-- Fade in the game over shade
			gameOverShade.isVisible = true
			gameOverShade.alpha = 0
			transition.to( gameOverShade, { time=500, x=240, alpha=1 } )

			if display.pixelHeight == 1136 then
				transition.to( gameOverShade, { time=500, x=300, alpha=1 } )
			end
			
			-- Slide the score banner from the right
			gameOverScoreBanner.isVisible = false
			transition.to( gameOverScoreBanner, { time=1000, x=240, transition=easing.inOutExpo } )

			if display.pixelHeight == 1136 then
				transition.to( gameOverScoreBanner, { time=500, x=300 } )
			end
			
			-- Update score and display label
			gameOverScoreText.text = scoreNumber
			gameOverScoreText.y = 75
			gameOverScoreText.alpha = 0
			gameOverScoreText.isVisible = true
			transition.to( gameOverScoreText, { time=2000, alpha=1, transition=easing.inOutExpo } )

			if display.pixelHeight == 1136 then
				transition.to( gameOverScoreText, { time=500, x=300 } )
			end
			
			-- Slide grass count banner down from the top
			grassScoreBg.isVisible = false
			transition.to( grassScoreBg, { time=500, y=24, transition=easing.inOutExpo } )
			
			-- Update grass score label
			local labelText = "Your Best Combo: " .. tostring(highestCombo)
			grassScoreText.text = labelText
			grassScoreText.y = 16
			grassScoreText.alpha = 0
			grassScoreText.isVisible = true
			transition.to( grassScoreText, { time=2000, alpha=1, transition=easing.inOutExpo } )

			if display.pixelHeight == 1136 then
				grassScoreText.x=300
			end
			
			-- Show "Try Again" Button
			tryAgainBtn.isVisible = true
			transition.to( tryAgainBtn, { time=2500, y=225, transition=easing.inOutExpo } )

			if display.pixelHeight == 1136 then
				tryAgainBtn.x=450
			end
			
			-- Show "Menu" Button
			menuBtn.isVisible = true
			transition.to( menuBtn, { time=2800, y=275, x=400,transition=easing.inOutExpo } )

			if display.pixelHeight == 1136 then
				transition.to( menuBtn, { time=2800, y=275, x=450,transition=easing.inOutExpo } )
			end

			if gameScore > 0 and gameScore < 1001 then
				calciumAward = display.newImageRect( "awards/calcium.png", 453, 297 )
				calciumAward.x = 240; calciumAward.y = 160
				calciumAward.isVisible = true

				if display.pixelHeight == 1136 then
					calciumAward.x=280; calciumAward.y = 160
				end

				function calciumAward:touch( event )
					display.remove( calciumAward );
				end

				calciumAward:addEventListener( "touch", calciumAward )

				gameGroup:insert( calciumAward )
			end

			if gameScore > 1000 and gameScore < 2001 then
				calciumAward = display.newImageRect( "awards/potassium.png", 453, 297 )
				calciumAward.x = 240; calciumAward.y = 160
				calciumAward.isVisible = true

				if display.pixelHeight == 1136 then
					calciumAward.x=280; calciumAward.y = 160
				end

				function calciumAward:touch( event )
					display.remove( calciumAward );
				end

				calciumAward:addEventListener( "touch", calciumAward )

				gameGroup:insert( calciumAward )
			end

			if gameScore > 2000 and gameScore < 3001 then
				calciumAward = display.newImageRect( "awards/phosphorus.png", 453, 297 )
				calciumAward.x = 240; calciumAward.y = 160
				calciumAward.isVisible = true

				if display.pixelHeight == 1136 then
					calciumAward.x=280; calciumAward.y = 160
				end

				function calciumAward:touch( event )
					display.remove( calciumAward );
				end

				calciumAward:addEventListener( "touch", calciumAward )

				gameGroup:insert( calciumAward )
			end

			if gameScore > 3000 and gameScore < 4001 then
				calciumAward = display.newImageRect( "awards/protein.png", 453, 297 )
				calciumAward.x = 240; calciumAward.y = 160
				calciumAward.isVisible = true

				if display.pixelHeight == 1136 then
					calciumAward.x=280; calciumAward.y = 160
				end

				function calciumAward:touch( event )
					display.remove( calciumAward );
				end

				calciumAward:addEventListener( "touch", calciumAward )

				gameGroup:insert( calciumAward )
			end

			if gameScore > 4000 and gameScore < 5001 then
				calciumAward = display.newImageRect( "awards/vitamind.png", 453, 297 )
				calciumAward.x = 240; calciumAward.y = 160
				calciumAward.isVisible = true

				if display.pixelHeight == 1136 then
					calciumAward.x=280; calciumAward.y = 160
				end

				function calciumAward:touch( event )
					display.remove( calciumAward );
				end

				calciumAward:addEventListener( "touch", calciumAward )

				gameGroup:insert( calciumAward )
			end

			if gameScore > 5000 and gameScore < 6001 then
				calciumAward = display.newImageRect( "awards/vitamina.png", 453, 297 )
				calciumAward.x = 240; calciumAward.y = 160
				calciumAward.isVisible = true

				if display.pixelHeight == 1136 then
					calciumAward.x=280; calciumAward.y = 160
				end

				function calciumAward:touch( event )
					display.remove( calciumAward );
				end

				calciumAward:addEventListener( "touch", calciumAward )

				gameGroup:insert( calciumAward )
			end

			if gameScore > 6000 and gameScore < 7001 then
				calciumAward = display.newImageRect( "awards/vitaminb12.png", 453, 297 )
				calciumAward.x = 240; calciumAward.y = 160
				calciumAward.isVisible = true

				if display.pixelHeight == 1136 then
					calciumAward.x=280; calciumAward.y = 160
				end

				function calciumAward:touch( event )
					display.remove( calciumAward );
				end

				calciumAward:addEventListener( "touch", calciumAward )

				gameGroup:insert( calciumAward )
			end

			if gameScore > 7000 and gameScore < 8001 then
				calciumAward = display.newImageRect( "awards/riboflavin.png", 453, 297 )
				calciumAward.x = 240; calciumAward.y = 160
				calciumAward.isVisible = true

				if display.pixelHeight == 1136 then
					calciumAward.x=280; calciumAward.y = 160
				end

				function calciumAward:touch( event )
					display.remove( calciumAward );
				end

				calciumAward:addEventListener( "touch", calciumAward )

				gameGroup:insert( calciumAward )
			end

			if gameScore > 8000 and gameScore < 24999 then
				calciumAward = display.newImageRect( "awards/niacin.png", 453, 297 )
				calciumAward.x = 240; calciumAward.y = 160
				calciumAward.isVisible = true

				if display.pixelHeight == 1136 then
					calciumAward.x=280; calciumAward.y = 160
				end

				function calciumAward:touch( event )
					display.remove( calciumAward );
				end

				calciumAward:addEventListener( "touch", calciumAward )

				gameGroup:insert( calciumAward )
			end

			if gameScore > 14999 then
				gameEnding = display.newImageRect ("game-ending-screen.png", 568, 320)
				gameEnding.x = 284; gameEnding.y = 160
				gameEnding.isVisible = true

				gameGroup:insert ( gameEnding )
				gameGroup:insert ( tryAgainBtn )
				gameGroup:insert ( menuBtn )

				function tryAgainBtn:touch( event )
					display.remove( gameEnding )
					recycleRound()
				end
			end

		end
		
		showGameOverScreen()

	end


	
	-- ************************************************************** --
	
	-- checkForGameOver() -- Check to see if player has lives left
	
	-- ************************************************************** --
	local checkForGameOver = function()
		
		if gameLives > 2 then
			gameLives = 2
		elseif gameLives <= 0 then
			callGameOver()
		end
	end	
	
	--***************************************************

	-- drawBackgound() --> create game background
	
	--***************************************************
	
	local drawBackground = function()
		local gameBackground

		gameBackground = display.newImageRect( "background.png", 568, 320 )
		gameBackground.x = 240; gameBackground.y = 160

		if display.pixelHeight == 1136 then
			gameBackground.x = 284
		end
		
		gameGroup:insert( gameBackground )
	end


	
	--***************************************************

	-- drawHighScore() --> create game background
	
	--***************************************************
	
	local drawHighScore = function()
		local highScore

		highScore = display.newText( "High Score:", 20, 05, "Helvetica-Bold", 12 )
		highScore:setTextColor(0, 0, 0)

		if display.pixelHeight == 1136 then
			highScore.x = 75
		end

		gameGroup:insert( highScore )
	end
	
	--***************************************************

	-- drawFence() --> create game background
	
	--***************************************************
	
	local drawFence = function()
		local picketFence

		picketFence = display.newImageRect( "picket-fence.png", 480, 320 )
		picketFence.x = 240; picketFence.y = 160
		
		gameGroup:insert( picketFence )
	end
	
	--***************************************************

	-- createPlayer() --> create the main character
	
	--***************************************************
	local createPlayer = function()
		-- first, create the electroBubble
		electroBubble = display.newImageRect( "electrobubble.png", 60, 60 )
		electroBubble.x = -100; electroBubble.y = -100
		electroBubble.isVisible = false
		
		local gameTheme = gameSettings["gameTheme"]
		local gameChar = gameSettings["gameChar"]

		-- gameTheme == "classic"
		-- gameChar == "d"
		
		if gameTheme == "classic" then
			
			if gameChar == "d" then
				playerObject = movieclip.newAnim({ "cow.png", "cow-gallup.png" }, 33, 83 )
				
			elseif gameChar == "ms. d" then
				playerObject = movieclip.newAnim({ "cow.png", "cow-gallup.png" }, 33, 83)
			
			elseif gameChar == "purple moe" then
				playerObject = movieclip.newAnim({ "cow.png", "cow-gallup.png" }, 33, 83)
			
			elseif gameChar == "green horn" then
				playerObject = movieclip.newAnim({ "cow.png", "cow-gallup.png" }, 33, 83 )
				
			end
		
		elseif gameTheme == "spook" then
			
			if gameChar == "d" then
				playerObject = movieclip.newAnim({ "cow.png", "cow-gallup.png" }, 33, 83 )
			
			elseif gameChar == "ms. d" then
				playerObject = movieclip.newAnim({ "cow.png", "cow-gallup.png" }, 33, 83 )
			
			elseif gameChar == "purple moe" then
				playerObject = movieclip.newAnim({ "cow.png", "cow-gallup.png" }, 33, 83 )
			
			elseif gameChar == "green horn" then
				playerObject = movieclip.newAnim({ "cow.png", "cow-gallup.png" }, 33, 83 )
				
			else
				playerObject = movieclip.newAnim({ "cow.png", "cow-gallup.png" }, 33, 83 )
			end
		
		end
		
		local theShape
		
		if gameChar == "d" or gameChar == "ms. d" then
			theShape = { -16,-41, 16,-41, 16, 41, -16,41 }
		elseif gameChar == "purple moe" then
			theShape = { -16,-41, 16,-41, 16, 41, -16,41 }
		elseif gameChar == "green horn" then
			theShape = { -16,-41, 16,-41, 16, 41, -16,41  }
		end
		
		physics.addBody( playerObject, "kinematic", { isSensor = true, density = 0, friction = 0, bounce = 0, shape = theShape, filter = playerFilter } )
		playerObject.isFixedRotation = true
		playerObject.myName = "player"
		playerObject.framePosition = "left"
		playerObject.animInterval = 200		--> lower = faster animation speed (old: 200)
		playerObject.isInvisible = false
		playerObject.isElectro = false
		playerObject.invisibleCycle = 1
		
		-- set actor speed multiplier based on tilt sensitivity (tiltSpeed)
		local tiltSpeed = gameSettings["tiltSpeed"]
		
		if tiltSpeed == "1" then
			playerObject.speed = 19
			
		elseif tiltSpeed == "2" then
			playerObject.speed = 22
			
		elseif tiltSpeed == "3" then
			playerObject.speed = 25
			
		elseif tiltSpeed == "4" then
			playerObject.speed = 32
			
		elseif tiltSpeed == "5" then
			playerObject.speed = 37
			
		else
			playerObject.speed = 25
		
		end
		
		local playerAnimation = function()
			if gameIsActive then
				if playerObject.framePosition == "left" then
					playerObject:stopAtFrame( 2 )
					playerObject.framePosition = "right"
				elseif playerObject.framePosition == "right" then
					playerObject:stopAtFrame( 1 )
					playerObject.framePosition = "left"
				end
			end
		end
		
		playerObject.animTimer = timer.performWithDelay( playerObject.animInterval, playerAnimation, 0 )	--> 250 is default interval
		
		-- Set player starting position on screen
		
		playerObject.x = 240
		playerObject.y = 125
		
		gameGroup:insert( electroBubble )
		gameGroup:insert( playerObject )
	end
	
	
	
	-- ************************************************************** --
	
	-- creategrasss() -- Create the grass objects
	
	-- ************************************************************** --
	local creategrasss = function()
		
		--************************************************************************
		
		-- writeTextAboveChar()	--> displaying floating/fading text
		
		--************************************************************************
		
		local writeTextAboveChar = function( textLineOne, charX, charY )
			
			local theString = textLineOne
			local theX = charX
			local theY = charY - 30
			
			floatingText.text = theString
			floatingText:setReferencePoint( display.CenterReferencePoint )
			floatingText:setTextColor( 66, 17, 148, 255 )
			floatingText.alpha = 1.0
			floatingText.isVisible = true
			floatingText.xOrigin = theX
			floatingText.yOrigin = theY
			
			local destroyMessage = function()
				floatingText.x = 500; floatingText.y = -100
				floatingText.alpha = 0
				floatingText.isVisible = false
			end
			
			local newY = theY - 100
			
			transition.to( floatingText, { time=500, alpha=0, y=newY, onComplete=destroyMessage } )
		end
		
		local ongrassCollision = function( self, event )
			if event.phase == "began" and event.other.myName == "player" then
				
				local doCollision = function()
					-- collision with player
					--self.isVisible = false
					self.alpha = 0
					
					local soundsOn = gameSettings[ "soundsOn" ]
						
					if soundsOn == true then
						local freeChan = audio.findFreeChannel()
						audio.play( grassSound, { channel=freeChan } )
					end
					
					--
					
					local scoreIncrease = 10 + (100 * grassCombo) + treeCycle
					grassCombo = grassCombo + 1
					grassCount = grassCount + 1
					
					if grassCombo >= 2 then
						local comboCount = tostring(grassCombo)
						
						comboText.text = "Combo " .. comboCount .. "x"
						comboText.isVisible = true
						comboIcon.isVisible = true
						comboBackground.isVisible = true
						
						-- set highest combo value
						if grassCombo > highestCombo then
							highestCombo = grassCombo
						end
						
						-- OpenFeint Achievements:
						if grassCombo >= 60 and ofAch[ "combo_60" ] == false then
							ofAch[ "combo_60" ] = true
						end
						
						if grassCombo >= 50 and ofAch[ "combo_50" ] == false then
							ofAch[ "combo_50" ] = true
						end
						
						if grassCombo >= 40 and ofAch[ "combo_40" ] == false then
							ofAch[ "combo_40" ] = true
						end
						
						if grassCombo >= 30 and ofAch[ "combo_30" ] == false then
							ofAch[ "combo_30" ] = true
						end
						
						if grassCombo >= 25 and ofAch[ "combo_25" ] == false then
							ofAch[ "combo_25" ] = true
						end
						
						if grassCombo >= 15 and ofAch[ "combo_15" ] == false then
							ofAch[ "combo_15" ] = true
						end
						
						if grassCombo >= 5 and ofAch[ "combo_5" ] == false then
							ofAch[ "combo_5" ] = true
						end
					end
					
					local currentScore = theScore + scoreIncrease
					setScore( currentScore )
					
					writeTextAboveChar( tostring(scoreIncrease), self.xOrigin, self.yOrigin )

					if currentScore > 14999 then
						gameIsActive = false
						local gameOverTimer = timer.performWithDelay( 200, callGameOver(), 1 )
					end
					
					self.isBodyActive = false
				end
				
				local collisionTimer = timer.performWithDelay( 1, doCollision, 1 )
			else
				return true
			end
		end
		
		local gameTheme = gameSettings["gameTheme"]
		
		if gameSettings["shouldOptimize"] == true then
			if random1to4Table[oneFourIndice] <= 2 then
				grassObject1 = movieclip.newAnim({ "grass1.png", "grass1.png", "grass2.png", "grass2.png" }, 28, 40 )
				grassObject2 = movieclip.newAnim({ "grass1.png", "grass1.png", "grass2.png", "grass2.png" }, 28, 28 )
			else
				grassObject1 = movieclip.newAnim({ "grass3.png", "grass3.png", "grass4.png", "grass4.png" }, 28, 28 )
				grassObject2 = movieclip.newAnim({ "grass3.png", "grass3.png", "grass4.png", "grass4.png" }, 28, 28 )
			end
		else
			grassObject1 = movieclip.newAnim({ "grass3.png", "grass1.png", "grass4.png", "grass2.png" }, 28, 28 )
			grassObject2 = movieclip.newAnim({ "grass3.png", "grass1.png", "grass4.png", "grass2.png" }, 28, 28 )
		end
		
		local theShape = { -9,-10, 9,-10, 9,10, -9,10 }
		physics.addBody( grassObject1, "dynamic", { isSensor = true, density = 0, friction = 0, bounce = 0, shape = theShape, filter = itemMonsterFilter } )
		physics.addBody( grassObject2, "dynamic", { isSensor = true, density = 0, friction = 0, bounce = 0, shape = theShape, filter = itemMonsterFilter } )
		
		
		grassObject1.isFixedRotation = true
		grassObject2.isFixedRotation = true
		grassObject1.alpha = 1.0
		grassObject2.alpha = 1.0
		grassObject1.myName = "grass1"
		grassObject2.myName = "grass2"
		
		-- set collisions
		grassObject1.collision = ongrassCollision
		grassObject2.collision = ongrassCollision
		
		grassObject1:addEventListener( "collision", grassObject1 )
		grassObject2:addEventListener( "collision", grassObject2 )
		
		-- set random color for both grass objects
		local randValue1 = random1to4Table[oneFourIndice]
		oneFourIndice = oneFourIndice + 1
		if oneFourIndice > maxOneFourIndice then
			oneFourIndice = 1
		end
		local randValue2 = random1to4Table[oneFourIndice]
		oneFourIndice = oneFourIndice + 1
		if oneFourIndice > maxOneFourIndice then
			oneFourIndice = 1
		end
		
		grassObject1:stopAtFrame( randValue1 )
		grassObject2:stopAtFrame( randValue2 )
		
		-- set initial location for both grass objects
		--grassObject1.x = mRandom( 70, 410 ); grassObject1.y = 420
		--grassObject2.x = mRandom( 70, 410 ); grassObject2.y = 620
		
		grassObject1.x = randomgrassLocations[grassIndice];
		grassIndice = grassIndice + 1
		if grassIndice > maxgrassIndice then
			grassIndice = 1
		end
		grassObject1.y = 420
		
		grassObject2.x = randomgrassLocations[grassIndice];
		grassIndice = grassIndice + 1
		if grassIndice > maxgrassIndice then
			grassIndice = 1
		end
		grassObject2.y = 620
		
		
		gameGroup:insert( grassObject1 )
		gameGroup:insert( grassObject2 )
	end
	
	-- ************************************************************** --
	
	-- movegrasss() -- move grasss based on game speed
	
	-- ************************************************************** --
	
	local movegrasss = function()
		
		local gameMoveSpeed = gameSettings[ "gameMoveSpeed" ]
		local randValue
		
		grassObject1.y = grassObject1.y - gameMoveSpeed
		grassObject2.y = grassObject2.y - gameMoveSpeed
		
		-- grassObjects go past player (reset grass combo)
		if grassObject1.y <= 60 then
			-- if grass wasn't picked up, reset combo count
			if grassObject1.alpha == 1.0 then
				grassCombo = 0
			end
		end
		
		if grassObject2.y <= 60 then
			-- if grass wasn't picked up, reset combo count
			if grassObject2.alpha == 1.0 then
				grassCombo = 0
			end
		end
		
		-- grassObject1 goes past screen (top)
		if grassObject1.y <= -32 then
		
			grassObject1.x = randomgrassLocations[grassIndice];
			grassIndice = grassIndice + 1
			if grassIndice > maxgrassIndice then
				grassIndice = 1
			end
			grassObject1.y = 420
			
			if grassObject1.isVisible == false or grassObject1.alpha == 0 then
				grassObject1.isVisible = true
				grassObject1.alpha = 1.0
			end
			
			-- choose new grass color
			-- set random color for both grass objects
			randValue = random1to4Table[oneFourIndice]
			oneFourIndice = oneFourIndice + 1
			if oneFourIndice > maxOneFourIndice then
				oneFourIndice = 1
			end
			
			grassObject1:stopAtFrame( randValue )
			
			grassObject1.isBodyActive = true
			
		end
		
		-- grassObject2 goes past screen (top)
		if grassObject2.y <= -32 then
			
			grassObject2.x = randomgrassLocations[grassIndice];
			grassIndice = grassIndice + 1
			if grassIndice > maxgrassIndice then
				grassIndice = 1
			end
			grassObject2.y = 420
			
			if grassObject2.isVisible == false or grassObject2.alpha == 0 then
				grassObject2.isVisible = true
				grassObject2.alpha = 1.0
			end
			
			-- choose new grass color
			randValue = random1to4Table[oneFourIndice]
			oneFourIndice = oneFourIndice + 1
			if oneFourIndice > maxOneFourIndice then
				oneFourIndice = 1
			end
			grassObject2:stopAtFrame( randValue )
			
			grassObject2.isBodyActive = true
		end
	end
	
	-- ************************************************************** --
	
	-- destroyAllEnemies() -- Destroy game-wise, not object-removal
	
	-- ************************************************************** --
	local destroyAllEnemies = function()
		
		if gameIsActive then
			-- Bunnies
			flyingBunny1.isBodyActive = false; flyingBunny1.isVisible = false
			flyingBunny2.isBodyActive = false; flyingBunny2.isVisible = false
			
			if gameSettings[ "difficulty" ] ~= "easy" then
				flyingBunny3.isBodyActive = false; flyingBunny3.isVisible = false
			end
			
			-- Bombs
			-- bombObject1.isBodyActive = false; bombObject1.isVisible = false
			
			-- Big Enemy
			BadFence.onTheMove = false; BadFence.isBodyActive = false; BadFence.isDestroyed = true; BadFence.isVisible = false
			
			-- milk
			milkObject.onTheMove = false; milkObject.isBodyActive = false; milkObject.isDestroyed = true; milkObject.isVisible = false
		end
	end
	
	-- ************************************************************** --
	
	-- createPickups() -- Create the pickup items
	
	-- ************************************************************** --
	local createPickups = function()
		local collideActions = function()
			
			pickupObject.onTheMove = false
			
			-- increment the score
			local currentScore = getScore()
			currentScore = currentScore + 300
			setScore( currentScore )
			
			-- do something based on WHAT pickup it is
			if pickupObject.myName == "fire" then
				-- Destroy all enemies
				destroyAllEnemies()
				dropNotification( "Enemies Destroyed +300" )
				
			elseif pickupObject.myName == "invisible" then
				-- Invisibility
				playerObject.isInvisible = true
				playerObject.alpha = 0.45
				playerObject.invisibleCycle = 1
				dropNotification( "Invincibility +300" )
				
			elseif pickupObject.myName == "electro" then
				-- Destroy on touch
				
				electroBubble.isVisible = true
				playerObject.isInvisible = true
				playerObject.invisibleCycle = 1
				playerObject.isElectro = true
				dropNotification( "Destroy on Contact +300" )
				
			elseif pickupObject.myName == "heart" then
				-- Increase Life
				gameLives = 2
				heartLeft.alpha = 1.0
				dropNotification( "Extra Heart Earned +300" )
				
			end
		end
		
		local onPickupCollision = function( self, event )
			if event.phase == "began" and event.other.myName == "player" and gameIsActive == true then
				
				local doCollision = function()
					self.isBodyActive = false
					self.isVisible = false
					
					-- collision with player
					local soundsOn = gameSettings[ "soundsOn" ]
					
					if soundsOn == true then
						local freeChan = audio.findFreeChannel()
						audio.play( pickupSound, { channel=freeChan } )
					end
					
					flashAnimationPickup( "pickup" )
					collideActions()
				end
				
				local collisionTimer = timer.performWithDelay( 1, doCollision, 1 )
			end
		end
		
		pickupObject = movieclip.newAnim({ "fire-pickup.png", "invisible-pickup.png", "electro-pickup.png", "heart-pickup.png" }, 30, 30 )
		
		local theRadius = 14
		physics.addBody( pickupObject, "dynamic", { isSensor = true, density = 0, friction = 0, bounce = 0, radius=theRadius, filter = itemMonsterFilter } )
		
		pickupObject.isFixedRotation = true
		pickupObject.myName = "fire"	--> fire, invisible, electro, heart
		pickupObject.onTheMove = false	--> when set to true, moves up like other game objects (otherwise, is on standby)
		pickupObject.isVisible = false
		
		-- set collisions
		pickupObject.collision = onPickupCollision
		pickupObject:addEventListener( "collision", pickupObject )
		
		-- set initial location for the pickup object
		
		pickupObject.x = randomgrassLocations[grassIndice];
		grassIndice = grassIndice + 1
		if grassIndice > maxgrassIndice then
			grassIndice = 1
		end
		
		pickupObject.y = 520
		
		
		gameGroup:insert( pickupObject )
		
	end
	
	-- ************************************************************** --
	
	-- movePickups() -- move pickups based on game speed
	
	-- ************************************************************** --
	
	local movePickups = function()
		
		if pickupObject.onTheMove then
			local gameMoveSpeed = gameSettings[ "gameMoveSpeed" ]
			
			pickupObject.isVisible = true
			pickupObject.isBodyActive = true
			
			pickupObject.y = pickupObject.y - gameMoveSpeed
			
			-- Pickup goes past screen (top)
			if pickupObject.y <= -34 then
				pickupObject.onTheMove = false
				pickupObject.isVisible = false
				pickupObject.isBodyActive = false
				
				pickupObject.x = randomgrassLocations[grassIndice];
				grassIndice = grassIndice + 1
				if grassIndice > maxgrassIndice then
					grassIndice = 1
				end
				pickupObject.y = 520
			end
		end
	end
	
	-- ************************************************************** --
	
	-- createCheckPoint() -- Create the checkpoint item
	
	-- ************************************************************** --
	local createCheckPoint = function()
		
		local onCheckPointCollision = function( self, event )
			if event.phase == "began" and event.other.myName == "player" then
				
				local doCollision = function()
					-- collision with player
					local soundsOn = gameSettings[ "soundsOn" ]
					
					if soundsOn == true then
						local freeChan = audio.findFreeChannel()
						audio.play( checkPointSound, { channel=freeChan } )
					end
					
					self.onTheMove = false
					self.isBodyActive = false
					
					local scoreIncrement = 3000
					
					-- increment the score
					local currentScore = getScore()
					
					-- count how many checkpoints were crossed
					cpCount = cpCount + 1
					
					-- OPENFEINT ACHIEVEMENTS
					if currentScore == 0 and cpCount == 1 and ofAch[ "zero_hero" ] == false then
						-- Unlock Zero Hero Achievement
						ofAch[ "zero_hero" ] = true
					end
					
					if currentScore <= 3000 and cpCount == 2 and ofAch[ "under_achiever" ] == false then
						-- Unlock Under Achiever Achievement
						ofAch[ "under_achiever" ] = true
					end
					
					if currentScore <= 6000 and cpCount == 3 and ofAch[ "grass_hater" ] == false then
						-- Unlock grass Hater Achievement
						ofAch[ "grass_hater" ] = true
					end
					
					if currentScore <= 9000 and cpCount == 4 and ofAch[ "slacker" ] == false then
						-- Unlock Slacker Achievement
						ofAch[ "slacker" ] = true
					end
					
					if currentScore <= 12000 and cpCount == 5 and ofAch[ "super_slacker" ] == false then
						-- Unlock Super Slacker Achievement
						ofAch[ "super_slacker" ] = true
					end
					
					-- END OPENFEINT ACHIEVEMENTS
					
					if cpCount < 5 then
						currentScore = currentScore + scoreIncrement
						
						-- Show notification message
						dropNotification( "Checkpoint Reached! +3000" )
					else
						scoreIncrement = 3000 * (cpCount - 3)
						currentScore = currentScore + scoreIncrement
						
						local notifyMessage = "Checkpoint Reached! +" .. tostring(scoreIncrement)
						
						-- Show notification message
						dropNotification( notifyMessage )
					end
					
					setScore( currentScore )
					
					
					-- make game faster
					if gameSettings[ "difficulty" ] == "easy" then
						gameSettings["gameMoveSpeed"] = gameSettings["gameMoveSpeed"] + 0.2	--> + 0.4 (30 fps) or 0.2 for (60 fps)
					
					elseif gameSettings[ "difficulty" ] == "medium" then
						gameSettings["gameMoveSpeed"] = gameSettings["gameMoveSpeed"] + 0.4	--> + 0.4 (30 fps) or 0.2 for (60 fps)
						
					elseif gameSettings[ "difficulty" ] == "medium" then
						gameSettings["gameMoveSpeed"] = gameSettings["gameMoveSpeed"] + 0.5	--> + 0.4 (30 fps) or 0.2 for (60 fps)
						
					end
					
					-- make player run faster
					if playerObject.animInterval > 100 then
						playerObject.animInterval = playerObject.animInterval - 20
					end
				end
				
				local collisionTimer = timer.performWithDelay( 1, doCollision, 1 )
			end
		end
		
		checkPointObject = display.newImageRect( "checkpoint.png", 480, 28 )
		
		physics.addBody( checkPointObject, "dynamic", { isSensor = true, density = 0, friction = 0, bounce = 0, filter = itemMonsterFilter } )
		
		checkPointObject.isFixedRotation = true
		checkPointObject.onTheMove = false	--> when set to true, moves up like other game objects (otherwise, is on standby)
		checkPointObject.isVisible = false
		checkPointObject.isBodyActive = false
		
		-- set collisions
		checkPointObject.collision = onCheckPointCollision
		checkPointObject:addEventListener( "collision", checkPointObject )
		
		-- set initial location for the pickup object
		
		checkPointObject.x = 240
		checkPointObject.y = 600
		
		
		gameGroup:insert( checkPointObject )
		
	end
	
	-- ************************************************************** --
	
	-- moveCheckPoint() -- move checkpoint based on game speed
	
	-- ************************************************************** --
	
	local moveCheckPoint = function()
		
		if checkPointObject.onTheMove then
			local gameMoveSpeed = gameSettings[ "gameMoveSpeed" ]
			
			checkPointObject.y = checkPointObject.y - gameMoveSpeed
			
			-- Pickup goes past screen (top)
			if checkPointObject.y <= -90 then
				checkPointObject.onTheMove = false
				checkPointObject.isVisible = false
				checkPointObject.isBodyActive = false
				
				checkPointObject.x = 240
				checkPointObject.y = 600
			end
		elseif checkPointObject.onTheMove == false and checkPointObject.y > -90 then
			local gameMoveSpeed = gameSettings[ "gameMoveSpeed" ]
			checkPointObject.y = checkPointObject.y - gameMoveSpeed
		elseif checkPointObject.onTheMove == false and checkPointObject.y < -90 then
			if checkPointObject.isVisible == true then
				checkPointObject.isVisible = false
			end
		end
	end
	
	-- ************************************************************** --
	
	-- createStar() -- Create the star object
	
	-- ************************************************************** --
	local createStar = function()
		
		--************************************************************************
		
		-- writeTextAboveChar()	--> displaying floating/fading text
		
		--************************************************************************
		
		local writeTextAboveChar = function( textLineOne, charX, charY )
			
			local theString = textLineOne
			local theX = charX
			local theY = charY - 30
			
			floatingTextStar.text = theString
			floatingTextStar:setReferencePoint( display.CenterReferencePoint )
			floatingTextStar:setTextColor( 66, 17, 148, 255 )
			floatingTextStar.alpha = 1.0
			floatingTextStar.isVisible = true
			floatingTextStar.xOrigin = theX
			floatingTextStar.yOrigin = theY
			
			local destroyMessage = function()
				floatingTextStar.x = 500; floatingTextStar.y = -100
				floatingTextStar.alpha = 0
				floatingTextStar.isVisible = false
			end
			
			local newY = theY - 100
			
			transition.to( floatingTextStar, { time=500, alpha=0, y=newY, onComplete=destroyMessage } )
		end
		
		local onStarCollision = function( self, event )
			if event.phase == "began" and event.other.myName == "player" then
				
				local doCollision = function( event )
					-- collision with player
					local soundsOn = gameSettings[ "soundsOn" ]
					
					if soundsOn == true then
						local freeChan = audio.findFreeChannel()
						audio.play( grassSound, { channel=freeChan } )
					end
					
					self.onTheMove = false
					self.isVisible = false
					self.isBodyActive = false
					
					-- increment the score
					local currentScore = getScore()
					local scoreIncrease = 10 * treeCycle
					currentScore = currentScore + scoreIncrease
					setScore( currentScore )
					
					-- OPENFEINT ACHIEVEMENT STUFF
					starCount = starCount + 1
					
					if starCount >= 15 and ofAch[ "fifteen_stars" ] == false then
						ofAch[ "fifteen_stars" ] = true
					end
					
					if starCount >= 25 and ofAch[ "star_struck" ] == false then
						ofAch[ "star_struck" ] = true
					end
					
					writeTextAboveChar( tostring(scoreIncrease), self.xOrigin, self.yOrigin )

					if currentScore > 24999 then
						gameIsActive = false
						local gameOverTimer = timer.performWithDelay( 200, callGameOver(), 1 )
					end
				end
				
				local collisionTimer = timer.performWithDelay( 1, doCollision, 1 )
			end
		end
		
		local gameTheme = gameSettings["gameTheme"]
		
		starObject = display.newImageRect( "hay.png", 36, 20 )
		
		local theRadius = 12
		physics.addBody( starObject, "dynamic", { isSensor = true, density = 0, friction = 0, bounce = 0, radius=theRadius, filter = itemMonsterFilter } )
		
		starObject.isFixedRotation = true
		starObject.onTheMove = false	--> when set to true, moves up like other game objects (otherwise, is on standby)
		starObject.isVisible = false
		
		-- set collisions
		starObject.collision = onStarCollision
		starObject:addEventListener( "collision", starObject )
		
		-- set initial location for the star object
		
		starObject.x = randomgrassLocations[grassIndice];
		grassIndice = grassIndice + 1
		if grassIndice > maxgrassIndice then
			grassIndice = 1
		end
		
		starObject.y = 720
		
		
		gameGroup:insert( starObject )
		
	end
	
	-- ************************************************************** --
	
	-- moveStar() -- move star based on game speed
	
	-- ************************************************************** --
	
	local moveStar = function()
		
		if starObject.onTheMove then
			local gameMoveSpeed = gameSettings[ "gameMoveSpeed" ]
			
			starObject.isVisible = true
			starObject.isBodyActive = true
			
			starObject.y = starObject.y - gameMoveSpeed
			
			-- Pickup goes past screen (top)
			if starObject.y <= -34 then
				starObject.onTheMove = false
				starObject.isVisible = false
				starObject.isBodyActive = false
				
				starObject.x = randomgrassLocations[grassIndice];
				grassIndice = grassIndice + 1
				if grassIndice > maxgrassIndice then
					grassIndice = 1
				end
				starObject.y = 720
				
			end
		end
	end
	
	
	-- ************************************************************** --
	
	-- createBunnies() -- Create the bunny enemies
	
	-- ************************************************************** --
	local createBunnies = function()
		
		local onBunnyCollision = function( self, event )
			if event.phase == "began" and event.other.myName == "player" then
				if playerObject.isInvisible == false then
					-- collision with player
					
					-- assess damage and lives
					if gameLives == 5 then
						local soundsOn = gameSettings[ "soundsOn" ]
						
						if soundsOn == true then
							local freeChan = audio.findFreeChannel()
							audio.play( hurtSound, { channel=freeChan } )
						end
						
						self.isBodyActive = false
					
						flashAnimation( "damage" )
						gameLives = 4; heartLeft4.alpha = .25

					elseif gameLives == 4 then
						local soundsOn = gameSettings[ "soundsOn" ]
						
						if soundsOn == true then
							local freeChan = audio.findFreeChannel()
							audio.play( hurtSound, { channel=freeChan } )
						end
						
						self.isBodyActive = false
					
						flashAnimation( "damage" )
						gameLives = 3; heartLeft4.alpha = .25; heartLeft3.alpha = .25

					elseif gameLives == 3 then
						local soundsOn = gameSettings[ "soundsOn" ]
						
						if soundsOn == true then
							local freeChan = audio.findFreeChannel()
							audio.play( hurtSound, { channel=freeChan } )
						end
						
						self.isBodyActive = false
					
						flashAnimation( "damage" )
						gameLives = 2; heartLeft4.alpha = .25; heartLeft3.alpha = .25; heartLeft2.alpha = .25

					elseif gameLives == 2 then
						local soundsOn = gameSettings[ "soundsOn" ]
						
						if soundsOn == true then
							local freeChan = audio.findFreeChannel()
							audio.play( hurtSound, { channel=freeChan } )
						end
						
						self.isBodyActive = false
					
						flashAnimation( "damage" )
						gameLives = 1; heartLeft4.alpha = .25; heartLeft3.alpha = .25; heartLeft2.alpha = .25; heartLeft.alpha = .25

					elseif gameLives == 1 then
						gameLives = 0
						gameIsActive = false
						
						local gameOverTimer = timer.performWithDelay( 200, callGameOver(), 1 )
					end
					
					return true
				
				else
					if playerObject.isElectro == true then
						local soundsOn = gameSettings[ "soundsOn" ]
						
						-- if soundsOn == true then
						-- 	local freeChan = audio.findFreeChannel()
						-- 	audio.play( bombSound, { channel=freeChan } )
						-- end
						
						self.isBodyActive = false; self.isVisible = false
						
						-- increment the score
						local currentScore = getScore()
						currentScore = currentScore + 200
						setScore( currentScore )
						
						return true
					else
						return true
					end
				end
				
				return true
			end
		end
		
		local gameTheme = gameSettings["gameTheme"]
		
		flyingBunny1 = movieclip.newAnim( { "tractor-1.png", "tractor-1.png" }, 50,36 )
		flyingBunny2 = movieclip.newAnim( { "tractor-2.png", "tractor-2.png" }, 50,36 )


		if gameSettings["shouldOptimize"] == true then
			flyingBunny3 = movieclip.newAnim( { "tractor-1.png", "tractor-1.png" }, 50,36 )
		else
			flyingBunny3 = movieclip.newAnim( { "tractor-2.png", "tractor-2.png" }, 50,36 )
		end
		
		local theShape = { -25,-18, 25,-18, 25,18,-25,18}
		physics.addBody( flyingBunny1, "dynamic", { isSensor = true, density = 0, friction = 0, bounce = 0, shape = theShape, filter = itemMonsterFilter } )
		physics.addBody( flyingBunny2, "dynamic", { isSensor = true, density = 0, friction = 0, bounce = 0, shape = theShape, filter = itemMonsterFilter } )
		
		if gameSettings[ "difficulty" ] ~= "easy" then
			physics.addBody( flyingBunny3, "dynamic", { isSensor = true, density = 0, friction = 0, bounce = 0, shape = theShape, filter = itemMonsterFilter } )
		end
		
		local flyingBunnyAnimInterval = 300
		
		flyingBunny1.isFixedRotation = true
		flyingBunny1.framePosition = 1
		flyingBunny2.isFixedRotation = true
		flyingBunny2.framePosition = 2
		
		if gameSettings[ "difficulty" ] == "easy" then
			flyingBunny1.selfSpeed = 0.4; flyingBunny1.dSelfSpeed = 0.4
			flyingBunny2.selfSpeed = 0.4; flyingBunny2.dSelfSpeed = 0.4
		
		elseif gameSettings[ "difficulty" ] == "medium" then
			flyingBunny1.selfSpeed = 0.5; flyingBunny1.dSelfSpeed = 0.5
			flyingBunny2.selfSpeed = 0.5; flyingBunny2.dSelfSpeed = 0.5
		
		elseif gameSettings[ "difficulty" ] == "hard" then
			flyingBunny1.selfSpeed = 0.6; flyingBunny1.dSelfSpeed = 0.6
			flyingBunny2.selfSpeed = 0.6; flyingBunny2.dSelfSpeed = 0.6
		
		end
		
		-- if gameSettings[ "difficulty" ] ~= "easy" then
		-- 	flyingBunny3.isFixedRotation = true
		-- 	flyingBunny3.framePosition = 1
		-- 	flyingBunny3.selfSpeed = 0.5
		-- end
		
		-- set collisions
		flyingBunny1.collision = onBunnyCollision
		flyingBunny2.collision = onBunnyCollision
		flyingBunny1:addEventListener( "collision", flyingBunny1 )
		flyingBunny2:addEventListener( "collision", flyingBunny2 )
		
		-- if gameSettings[ "difficulty" ] ~= "easy" then
		-- 	flyingBunny3.collision = onBunnyCollision
		-- 	flyingBunny3:addEventListener( "collision", flyingBunny3 )
		-- end
		
		-- set initial location for bunny objects
		--flyingBunny1.x = mRandom( 1, 479 ); flyingBunny1.y = 500
		--flyingBunny2.x = mRandom( 1, 479 ); flyingBunny2.y = 800
		flyingBunny1.x = randomBunnyLocations[bunnyIndice];
		bunnyIndice = bunnyIndice + 1;
		if bunnyIndice > maxBunnyIndice then
			bunnyIndice = 1
		end
		flyingBunny1.y = 500
		
		flyingBunny2.x = randomBunnyLocations[bunnyIndice];
		bunnyIndice = bunnyIndice + 1;
		if bunnyIndice > maxBunnyIndice then
			bunnyIndice = 1
		end
		flyingBunny2.y = 800
		
		if gameSettings[ "difficulty" ] ~= "easy" then
			flyingBunny3.x = randomBunnyLocations[bunnyIndice];
			bunnyIndice = bunnyIndice + 1;
			if bunnyIndice > maxBunnyIndice then
				bunnyIndice = 1
			end
			flyingBunny3.y = 950
		end
		
		-- handle bunny animations
		local bunny1Animation = function()
			if gameIsActive then
				if flyingBunny1.framePosition == 1 then
					flyingBunny1:stopAtFrame( 2 )
					flyingBunny1.framePosition = 2
				elseif flyingBunny1.framePosition == 2 then
					flyingBunny1:stopAtFrame( 1 )
					flyingBunny1.framePosition = 1
				end
			end
		end
		
		flyingBunny1.animTimer = timer.performWithDelay( flyingBunnyAnimInterval, bunny1Animation, 0 )
		
		local bunny2Animation = function()
			if gameIsActive then
				if flyingBunny2.framePosition == 1 then
					flyingBunny2:stopAtFrame( 2 )
					flyingBunny2.framePosition = 2
				elseif flyingBunny2.framePosition == 2 then
					flyingBunny2:stopAtFrame( 1 )
					flyingBunny2.framePosition = 1
				end
			end
		end
		
		flyingBunny2.animTimer = timer.performWithDelay( flyingBunnyAnimInterval, bunny2Animation, 0 )
		
		if gameSettings[ "difficulty" ] ~= "easy" then
			local bunny3Animation = function()
				if gameIsActive then
					if flyingBunny3.framePosition == 1 then
						flyingBunny3:stopAtFrame( 2 )
						flyingBunny3.framePosition = 2
					elseif flyingBunny3.framePosition == 2 then
						flyingBunny3:stopAtFrame( 1 )
						flyingBunny3.framePosition = 1
					end
				end
			end
			
			flyingBunny3.animTimer = timer.performWithDelay( flyingBunnyAnimInterval, bunny3Animation, 0 )
		end
		
		gameGroup:insert( flyingBunny1 )
		gameGroup:insert( flyingBunny2 )
		
		-- if gameSettings[ "difficulty" ] ~= "easy" then
		-- 	gameGroup:insert( flyingBunny3 )
		-- end
	end
	
	-- ************************************************************** --
	
	-- moveBunnies() -- move bunnies based on game speed
	
	-- ************************************************************** --
	
	local moveBunnies = function()
		
		local gameMoveSpeed = gameSettings[ "gameMoveSpeed" ]
		
		-- move bunny upward on screen
		flyingBunny1.y = flyingBunny1.y - (gameMoveSpeed + flyingBunny1.selfSpeed)
		flyingBunny2.y = flyingBunny2.y - (gameMoveSpeed + flyingBunny2.selfSpeed)
		
		if gameSettings[ "difficulty" ] ~= "easy" then
			flyingBunny3.y = flyingBunny3.y - (gameMoveSpeed + flyingBunny3.selfSpeed)
		end
		
		-- move bunny toward's character
		if playerObject.x < flyingBunny1.x then
			flyingBunny1.x = flyingBunny1.x - (gameMoveSpeed * 0.15)	--> 0.15 / 0.12
			
		elseif playerObject.x > flyingBunny1.x then
			flyingBunny1.x = flyingBunny1.x + (gameMoveSpeed * 0.15)	--> 0.15 / 0.12
			
		end
		
		if playerObject.x < flyingBunny2.x then
			flyingBunny2.x = flyingBunny2.x - (gameMoveSpeed * 0.07)	--> 0.07 / 0.04
			
		elseif playerObject.x > flyingBunny2.x then
			flyingBunny2.x = flyingBunny2.x + (gameMoveSpeed * 0.07)	--> 0.07 / 0.04
			
		end
		
		if gameSettings[ "difficulty" ] ~= "easy" then
			if playerObject.x < flyingBunny3.x then 
				flyingBunny3.x = flyingBunny3.x - (gameMoveSpeed * 0.11)	--> 0.11 / 0.08
				
			elseif playerObject.x > flyingBunny3.x then
				flyingBunny3.x = flyingBunny3.x + (gameMoveSpeed * 0.11)	--> 0.11 / 0.08
				
			end
		end
		
		-- once player passes bunny
		local frontOfPlayer = playerObject.y + 50
		
		if frontOfPlayer >= flyingBunny1.y then
			flyingBunny1.selfSpeed = -2.0	--> -2.0 / -1.0
		end
		
		if frontOfPlayer >= flyingBunny2.y then
			flyingBunny2.selfSpeed = -2.0	--> -2.0 / -1.0
		end
		
		if gameSettings[ "difficulty" ] ~= "easy" then
			if frontOfPlayer >= flyingBunny3.y then
				flyingBunny3.selfSpeed = -2.0	--> -2.0 / -1.0
			end
		end
		
		-- bunny moves past top of screen
		if flyingBunny1.y <= -58 then
			flyingBunny1.x = randomBunnyLocations[bunnyIndice]
			bunnyIndice = bunnyIndice + 1;
			if bunnyIndice > maxBunnyIndice then
				bunnyIndice = 1
			end
			flyingBunny1.y = 500
			
			flyingBunny1.selfSpeed = flyingBunny1.dSelfSpeed
			
			if flyingBunny1.isVisible == false then
				flyingBunny1.isVisible = true
			end
			
			flyingBunny1.isBodyActive = true
		end
		
		if flyingBunny2.y <= -58 then
			flyingBunny2.x = randomBunnyLocations[bunnyIndice]
			bunnyIndice = bunnyIndice + 1;
			if bunnyIndice > maxBunnyIndice then
				bunnyIndice = 1
			end
			flyingBunny2.y = 500
			flyingBunny2.selfSpeed = flyingBunny2.dSelfSpeed
			
			if flyingBunny2.isVisible == false then
				flyingBunny2.isVisible = true
			end
			
			flyingBunny2.isBodyActive = true
		end
		
		if gameSettings[ "difficulty" ] ~= "easy" then
			if flyingBunny3.y <= -58 then
				flyingBunny3.x = randomBunnyLocations[bunnyIndice]
				bunnyIndice = bunnyIndice + 1;
				if bunnyIndice > maxBunnyIndice then
					bunnyIndice = 1
				end
				flyingBunny3.y = 500
				flyingBunny3.selfSpeed = 0.5
				if flyingBunny3.isVisible == false then
					flyingBunny3.isVisible = false
				end
				
				flyingBunny3.isBodyActive = false
			end
		end
		
	end
	
	
	
	-- ************************************************************** --
	
	-- createBadFence() -- Create the big enemy object
	
	-- ************************************************************** --
	local createBadFence = function()
		
		local onBadFenceCollision = function( self, event )
			if event.phase == "began" and event.other.myName == "player" then
				local doCollision = function()
					if playerObject.isInvisible == false then
						-- collision with player
						
						if gameLives == 5 then
							
							self.onTheMove = false
							self.isBodyActive = false
							
							flashAnimation( "damage" )
							gameLives = 4; heartLeft4.alpha = .25; 
							
							local soundsOn = gameSettings[ "soundsOn" ]
							
							if soundsOn == true then
								local freeChan = audio.findFreeChannel()
								audio.play( hurtSound, { channel=freeChan } )
							end
							
							-- change the score (big enemy steals 1000 points)
							local currentScore = getScore()
							
							if gameSettings[ "difficulty" ] ~= "easy" then
								currentScore = currentScore - 1000
							else
								currentScore = currentScore - 400
							end
							
							if currentScore < 0 then currentScore = 0; end
							setScore( currentScore )

							local gameEnding = function ()
								if gameScore >= 10 then
									gameIsActive = false
									local gameOverTimer = timer.performWithDelay( 0, callGameOver(), 1 )
								end
							end
						
						elseif gameLives == 4 then
							
							self.onTheMove = false
							self.isBodyActive = false
							
							flashAnimation( "damage" )
							gameLives = 3; heartLeft4.alpha = .25; heartLeft3.alpha = .25; 
							
							local soundsOn = gameSettings[ "soundsOn" ]
							
							if soundsOn == true then
								local freeChan = audio.findFreeChannel()
								audio.play( hurtSound, { channel=freeChan } )
							end
							
							-- change the score (big enemy steals 1000 points)
							local currentScore = getScore()
							
							if gameSettings[ "difficulty" ] ~= "easy" then
								currentScore = currentScore - 1000
							else
								currentScore = currentScore - 400
							end
							
							if currentScore < 0 then currentScore = 0; end
							setScore( currentScore )
						
						elseif gameLives == 3 then
							
							self.onTheMove = false
							self.isBodyActive = false
							
							flashAnimation( "damage" )
							gameLives = 2; heartLeft4.alpha = .25; heartLeft3.alpha = .25; heartLeft2.alpha = .25;
							
							local soundsOn = gameSettings[ "soundsOn" ]
							
							if soundsOn == true then
								local freeChan = audio.findFreeChannel()
								audio.play( hurtSound, { channel=freeChan } )
							end
							
							-- change the score (big enemy steals 1000 points)
							local currentScore = getScore()
							
							if gameSettings[ "difficulty" ] ~= "easy" then
								currentScore = currentScore - 1000
							else
								currentScore = currentScore - 400
							end
							
							if currentScore < 0 then currentScore = 0; end
							setScore( currentScore )

						elseif gameLives == 2 then
							
							self.onTheMove = false
							self.isBodyActive = false
							
							flashAnimation( "damage" )
							gameLives = 1; heartLeft.alpha = .25; heartLeft2.alpha = .25; heartLeft3.alpha = .25; heartLeft4.alpha = .25;
							
							local soundsOn = gameSettings[ "soundsOn" ]
							
							if soundsOn == true then
								local freeChan = audio.findFreeChannel()
								audio.play( hurtSound, { channel=freeChan } )
							end
							
							-- change the score (big enemy steals 1000 points)
							local currentScore = getScore()
							
							if gameSettings[ "difficulty" ] ~= "easy" then
								currentScore = currentScore - 1000
							else
								currentScore = currentScore - 400
							end
							
							if currentScore < 0 then currentScore = 0; end
							setScore( currentScore )
							
						elseif gameLives == 1 then
							gameLives = 0
							--checkForGameOver()
							gameIsActive = false
							local gameOverTimer = timer.performWithDelay( 0, callGameOver(), 1 )
						end
						
						return true
						
					end
				end
				
				local collisionTimer = timer.performWithDelay( 1, doCollision, 1 )
			end
		end
		
		local gameTheme = gameSettings["gameTheme"]
		
		BadFence = display.newImageRect( "fence.png", 70, 37 )
		
		local theShape = { -35,-18, 35,-18, 35,18 -35,18}
		physics.addBody( BadFence, "dynamic", { isSensor = true, density = 0, friction = 0, bounce = 0, shape = theShape, filter = itemMonsterFilter } )
		
		BadFence.isFixedRotation = false
		BadFence.onTheMove = false	--> when set to true, moves up like other game objects (otherwise, is on standby)
		BadFence.isVisible = false
		BadFence.isDestroyed = false
		
		-- set collisions
		BadFence.collision = onBadFenceCollision
		BadFence:addEventListener( "collision", BadFence )
		
		-- set initial location for big enemy object
		BadFence.x = randomBunnyLocations[bunnyIndice]
		bunnyIndice = bunnyIndice + 1;
		if bunnyIndice > maxBunnyIndice then
			bunnyIndice = 1
		end
		BadFence.y = 320
		
		
		gameGroup:insert( BadFence )
	end
	
	-- ************************************************************** --
	
	-- moveBadFence() -- move big enemy based on game speed
	
	-- ************************************************************** --
	
	local moveBadFence = function()
		
		if BadFence.onTheMove then
			local gameMoveSpeed = gameSettings[ "gameMoveSpeed" ]
			
			BadFence.isVisible = true
			BadFence.isBodyActive = true
			
			BadFence.y = BadFence.y - gameMoveSpeed
			
		elseif BadFence.onTheMove == false and BadFence.isDestroyed == false and BadFence.y > -102 then
			local gameMoveSpeed = gameSettings[ "gameMoveSpeed" ]
			BadFence.y = BadFence.y - gameMoveSpeed
			BadFence.isBodyActive = false
		end
		
		if BadFence.isDestroyed then
			local gameMoveSpeed = gameSettings[ "gameMoveSpeed" ]
			BadFence.y = BadFence.y - gameMoveSpeed
			BadFence.isVisible = false
			BadFence.isBodyActive = false
		end
		
		-- Pickup goes past screen (top)
		if BadFence.y <= -102 then
			BadFence.onTheMove = false
			BadFence.isVisible = false
			BadFence.isBodyActive = false
			BadFence.isDestroyed = false
			
			BadFence.x = randomBunnyLocations[bunnyIndice];
			bunnyIndice = bunnyIndice + 1
			if bunnyIndice > maxBunnyIndice then
				bunnyIndice = 1
			end
			BadFence.y = 320
			
		end
	end
	
	-- ************************************************************** --
	
	-- createmilk() -- Create the milk object
	
	-- ************************************************************** --
	local createmilk = function()
		
		local onmilkCollision = function( self, event )
			if event.phase == "began" and event.other.myName == "player" then
				local doCollision = function()
					if playerObject.isInvisible == false then
						-- collision with player
						
						if gameLives == 4 then
							
							self.onTheMove = false
							self.isBodyActive = false
							self.alpha = 0

							flashAnimation( "pickup" ); heartLeft.alpha = 1; heartLeft2.alpha = 1; heartLeft3.alpha = 1; heartLeft4.alpha = 1;
							
							gameLives = 5
						
						elseif gameLives == 3 then
							
							self.onTheMove = false
							self.isBodyActive = false
							self.alpha = 0

							flashAnimation( "pickup" ); heartLeft.alpha = 1; heartLeft2.alpha = 1; heartLeft3.alpha = 1; 
							
							gameLives = 4
						
						elseif gameLives == 2 then
							
							self.onTheMove = false
							self.isBodyActive = false
							self.alpha = 0

							flashAnimation( "pickup" ); heartLeft.alpha = 1; heartLeft2.alpha = 1;
							
							gameLives = 3
						
						elseif gameLives == 1 then
							
							self.onTheMove = false
							self.isBodyActive = false
							self.alpha = 0

							flashAnimation( "pickup" ); heartLeft.alpha = 1
							
							gameLives = 2
							
						elseif gameLives == 5 then
							gameLives = 5
						end
						
						return true

					end
				end
				
				local collisionTimer = timer.performWithDelay( 1, doCollision, 1 )
			end
		end
		
		local gameTheme = gameSettings["gameTheme"]
		
		milkObject = display.newImageRect( "extraLife.png", 23, 40 )
		
		local theShape = { -12,-20, 12,-20, 12,20 -12,20}
		physics.addBody( milkObject, "dynamic", { isSensor = true, density = 0, friction = 0, bounce = 0, shape = theShape, filter = itemMonsterFilter } )
		
		milkObject.isFixedRotation = true
		milkObject.onTheMove = false	--> when set to true, moves up like other game objects (otherwise, is on standby)
		milkObject.isVisible = false
		milkObject.isDestroyed = false
		
		-- set collisions
		milkObject.collision = onmilkCollision
		milkObject:addEventListener( "collision", milkObject )
		
		-- set initial location for big enemy object
		milkObject.x = randomBunnyLocations[bunnyIndice]
		bunnyIndice = bunnyIndice + 1;
		if bunnyIndice > maxBunnyIndice then
			bunnyIndice = 1
		end
		milkObject.y = 300
		
		
		gameGroup:insert( milkObject )
	end
	
	-- ************************************************************** --
	
	-- moveMilk() -- move milk based on game speed
	
	-- ************************************************************** --
	
	local moveMilk = function()
		
		if milkObject.onTheMove then
			local gameMoveSpeed = gameSettings[ "gameMoveSpeed" ]
			
			milkObject.isVisible = true
			milkObject.isBodyActive = true
			
			milkObject.y = milkObject.y - gameMoveSpeed
			
		elseif milkObject.onTheMove == false and milkObject.isDestroyed == false and milkObject.y > -84 then
			local gameMoveSpeed = gameSettings[ "gameMoveSpeed" ]
			milkObject.y = milkObject.y - gameMoveSpeed
			milkObject.isBodyActive = false
		end
		
		if milkObject.isDestroyed then
			local gameMoveSpeed = gameSettings[ "gameMoveSpeed" ]
			milkObject.y = milkObject.y - gameMoveSpeed
			milkObject.isVisible = false
			milkObject.isBodyActive = false
		end
		
		-- milk goes past screen (top)
		if milkObject.y <= -84 then
			milkObject.onTheMove = false
			milkObject.isVisible = false
			milkObject.isBodyActive = false
			milkObject.isDestroyed = false
			
			milkObject.x = randomBunnyLocations[bunnyIndice];
			bunnyIndice = bunnyIndice + 1
			if bunnyIndice > maxBunnyIndice then
				bunnyIndice = 1
			end
			milkObject.y = 500
			
		end
	end
	
	-- ************************************************************** --
	
	-- checkObjectSpawn() -- spawn certain objects on new tree cycle
	
	-- ************************************************************** --
	local checkObjectSpawn = function()
		
		-- CHECKPOINT
		if checkPointCycle >= checkPointSpawnRate then
			checkPointCycle = 1		--> reset pickup cycle
			
			checkPointObject.x = 240
			checkPointObject.y = 600
			checkPointObject.onTheMove = true
			checkPointObject.isVisible = true
			checkPointObject.isBodyActive = true
		end
		
		-- STAR ITEM
		if starCycle >= starSpawnRate then
			starCycle = 1		--> reset star cycle
			
			starObject.x = randomgrassLocations[grassIndice];
			grassIndice = grassIndice + 1
			if grassIndice > maxgrassIndice then
				grassIndice = 1
			end
			starObject.y = 720
			starObject.onTheMove = true
		end
		
		-- BIG ENEMY
		if BadFenceCycle >= BadFenceSpawnRate then
			BadFenceCycle = 2		--> reset big enemy cycle
			
			BadFence.x = randomBunnyLocations[bunnyIndice];
			bunnyIndice = bunnyIndice + 1
			if bunnyIndice > maxBunnyIndice then
				bunnyIndice = 2
			end
			BadFence.y = 900
			BadFence.onTheMove = true
		end
		
		-- GREEN milk
		if milkCycle >= milkSpawnRate then
			milkCycle = 2		--> reset big enemy cycle
			
			milkObject.x = randomBunnyLocations[bunnyIndice];
			bunnyIndice = bunnyIndice + 1
			if bunnyIndice > maxBunnyIndice then
				bunnyIndice = 2
			end
			milkObject.y = 500
			milkObject.onTheMove = true
		end
	end

	
	-- ************************************************************** --
	
	-- creatFence() -- Draw trees
	
	-- ************************************************************** --
	local creatFence = function()
		
		local gameTheme = gameSettings["gameTheme"]
		
		treeObjects["left1"] = display.newImageRect( "dotted-border.png", 3, 640 )
		treeObjects["left2"] = display.newImageRect( "dotted-border.png", 3, 640 )
		treeObjects["right1"] = display.newImageRect( "dotted-border.png", 3, 640 )
		treeObjects["right2"] = display.newImageRect( "dotted-border.png", 3, 640 )
		
		treeObjects["left1"].x = 10 
		if display.pixelHeight == 1136 then
			treeObjects["left1"].x = 30
		end
		treeObjects["left1"].y = 0
		treeObjects["left2"].x = treeObjects["left1"].x
		treeObjects["left2"].y = 640
		
		treeObjects["right1"].x = 470 

		if display.pixelHeight == 1136 then
			treeObjects["right1"].x = 540
		end
		treeObjects["right1"].y = 0
		treeObjects["right2"].x = treeObjects["right1"].x
		treeObjects["right2"].y = 640
		
		gameGroup:insert( treeObjects["left1"] )
		gameGroup:insert( treeObjects["left2"] )
		gameGroup:insert( treeObjects["right1"] )
		gameGroup:insert( treeObjects["right2"] )
	end
	
	-- ************************************************************** --

	-- moveTrees() -- move trees upward

	-- ************************************************************** --
	local moveTrees = function()
		local gameMoveSpeed = gameSettings[ "gameMoveSpeed" ]
		
		treeObjects["left1"].y = treeObjects["left1"].y - (gameMoveSpeed * 0.35)
		treeObjects["left2"].y = treeObjects["left2"].y - (gameMoveSpeed * 0.35)
		treeObjects["right1"].y = treeObjects["right1"].y - (gameMoveSpeed * 0.35)
		treeObjects["right2"].y = treeObjects["right2"].y - (gameMoveSpeed * 0.35)
		
		-- move trees back to the bottom once they go too far up
		-- also, increment score by 10 everytime the first tree cycles
		
		if treeObjects["left1"].y < -640 then
			treeObjects["left1"].y = 640
			
			--[[
			local currentScore = getScore()
			currentScore = currentScore + 10
			setScore( currentScore )
			]]--
			
			treeCycle = treeCycle + 1
			pickupCycle = pickupCycle + 1
			checkPointCycle = checkPointCycle + 1
			starCycle = starCycle + 1
			BadFenceCycle = BadFenceCycle + 2
			milkCycle = milkCycle + 2
			
			checkObjectSpawn()
			
			-- if in easy mode, increment score
			if gameSettings[ "difficulty" ] == "easy" then
				local gameScore = getScore()
				gameScore = gameScore + 25
				setScore( gameScore )
			end
			
			-- Handle player invisibility
			if playerObject.isInvisible == true then
				playerObject.invisibleCycle = playerObject.invisibleCycle + 1
				
				if playerObject.invisibleCycle >= 5 then
					playerObject.isInvisible = false
					playerObject.isElectro = false
					electroBubble.isVisible = false; electroBubble.x = -100; electroBubble.y = -100
					playerObject.alpha = 1.0
				end
			end
			
			-- See if combo display needs to be hidden
			if grassCombo < 2 then
				comboText.isVisible = false
				comboIcon.isVisible = false
				comboBackground.isVisible = false
			end
		end
		
		if treeObjects["left2"].y < -640 then
			treeObjects["left2"].y = 640
			
			-- if in easy mode, increment score
			if gameSettings[ "difficulty" ] == "easy" then
				local gameScore = getScore()
				gameScore = gameScore + 25
				setScore( gameScore )
			end
			
			-- See if combo display needs to be hidden
			if grassCombo < 2 then
				comboText.isVisible = false
				comboIcon.isVisible = false
				comboBackground.isVisible = false
			end
		end
		
		if treeObjects["right1"].y < -638 then
			treeObjects["right1"].y = 640
		end
		
		if treeObjects["right2"].y < -638 then
			treeObjects["right2"].y = 640
		end
end
	
	
	-- ************************************************************** --
	
	-- drawHUD() -- Draw things on the heads up display
	
	-- ************************************************************** --
	local drawHUD = function()
		local scoreInfo = getInfo()
		local border = 5
		
		-- Combo Background
		comboBackground = display.newRoundedRect( 30, 31, 122, 28, 5 )
		comboBackground:setFillColor( 255, 255, 255, 255 )
		comboBackground.alpha = 0.80
		comboBackground.isVisible = false
		
		-- Combo Icon
		comboIcon = display.newImageRect( "comboicon.png", 18, 16 )
		comboIcon.x = 48; comboIcon.y = 45
		comboIcon.isVisible = false
		
		-- Combo text
		comboText = display.newText( "Combo 2x", 400, 277, "Helvetica-Bold", 28 )
		comboText:setTextColor( 0, 0, 0, 255 )
		comboText.xScale = 0.5; comboText.yScale = 0.5
		comboText.x = 103; comboText.y = 46
		comboText.isVisible = false
		
		-- Notification Banner
		notificationBanner = display.newImageRect( "notification-banner.png", 608, 30 )
		--notificationBanner:setReferencePoint( display.TopCenterReferencePoint )
		notificationBanner.x = -130; notificationBanner.y = -32
		notificationBanner.isVisible = false
		
		-- Notification Text
		notificationText = display.newText( " ", 30, -16, "Helvetica-Bold", 13 )
		notificationText:setTextColor( 255, 255, 255, 255 )
		notificationText:setReferencePoint( display.CenterLeftReferencePoint )
		notificationText.x = 34; notificationText.y = -18
		notificationText.isVisible = false
		
		-- Score Init
		--x = scoreInfo.width - ((scoreInfo.width + border) * 2)
		init( { 
			x = 10, 
			y = 272 }
		)
		setScore( 0 )
		theBackground.isVisible = true
		
		-- Round border
		local roundBorder = display.newImageRect( "white-top-border.png", 580, 24 )
		roundBorder.x = 240; roundBorder.y = 12

		if display.pixelHeight == 1136 then
			roundBorder.x = 280
		end
		
		-- Difficulty display
		local diffText = display.newText( "Medium", 350, 310, "Helvetica-Bold", 32 )
		diffText:setTextColor( 252, 255, 255, 255 )
		if gameSettings[ "difficulty" ] == "easy" then
			diffText.text = ""
		elseif gameSettings[ "difficulty" ] == "medium" then
			diffText.text = ""
		elseif gameSettings[ "difficulty" ] == "hard" then
			diffText.text = ""
		end
		diffText.xScale = 0.5; diffText.yScale = 0.5
		diffText.x = 460 - (diffText.contentWidth / 2); diffText.y = 310
		
		-- Heart Background
		-- heartBackground = display.newImageRect( "heartbg.png", 88, 42 )
		-- heartBackground.x = 430; heartBackground.y = 28
		
		-- Heart Display
		heartLeft = display.newImageRect( "milkHeart.png", 9, 15 )
		heartLeft.x = 428; heartLeft.y = 14

		heartLeft2 = display.newImageRect( "milkHeart.png", 9, 15 )
		heartLeft2.x = 408; heartLeft2.y = 14

		heartLeft3 = display.newImageRect( "milkHeart.png", 9, 15 )
		heartLeft3.x = 388; heartLeft3.y = 14

		heartLeft4 = display.newImageRect( "milkHeart.png", 9, 15 )
		heartLeft4.x = 368; heartLeft4.y = 14
		
		heartRight = display.newImageRect( "milkHeart.png", 9, 15 )
		heartRight.x = 448; heartRight.y = 14

		if display.pixelHeight == 1136 then
			heartLeft.x = 458; heartRight.x = 478; heartLeft2.x = 438; heartLeft3.x = 418; heartLeft4.x = 398
		end
		
		-- Damage Rectangle
		damageRect = display.newRect( 0, 0, display.contentWidth, display.contentHeight )
		damageRect:setFillColor( 172, 0, 0, 255 )
		damageRect.isVisible = false
		damageRect.alpha = 0
		
		-- Pickup Rectangle
		pickupRect = display.newRect( 0, 0, display.contentWidth, display.contentHeight )
		pickupRect:setFillColor( 255, 255, 255, 255 )
		pickupRect.isVisible = false
		pickupRect.alpha = 0
		
		-- Best Score Text Display (upper left corner)
		local theBestScore = comma_value( gameSettings["bestScore"] )
		bestScoreText = display.newText( theBestScore, 25, 10, "Helvetica-Bold", 30 )
		
		-- For displaying final image optimization count:
		--[[
		local optimCount = loadValue( "optim.data" )
		bestScoreText = display.newText( optimCount, 25, 10, "Helvetica-Bold", 15 )
		]]--
		
		bestScoreText:setTextColor( 0, 0, 0, 255 )
		--bestScoreText:setReferencePoint( display.CenterLeftReferencePoint )
		--bestScoreText.x = 25; bestScoreText.y = 11
		bestScoreText.xScale = 0.5; bestScoreText.yScale = 0.5
		bestScoreText.x = (bestScoreText.contentWidth / 2) + 25; bestScoreText.y = 10
		
		-- Floating Text Display and Floating Text Star and Floating Text Big Enemy
		floatingText = display.newText( "", 500, -100, "Helvetica-Bold", 15 )
		floatingText.alpha = 0
		floatingText.isVisible = false
		
		floatingTextStar = display.newText( "", 500, -100, "Helvetica-Bold", 15 )
		floatingTextStar.alpha = 0
		floatingTextStar.isVisible = false
		
		
		gameGroup:insert( floatingText )
		gameGroup:insert( floatingTextStar )
		gameGroup:insert( damageRect )
		gameGroup:insert( pickupRect )
		gameGroup:insert( comboBackground )
		gameGroup:insert( comboIcon )
		gameGroup:insert( comboText )
		gameGroup:insert( roundBorder )
		gameGroup:insert( diffText )
		gameGroup:insert( notificationBanner )
		gameGroup:insert( notificationText )
		gameGroup:insert( theScoreGroup )
		--gameGroup:insert( heartBackground )
		gameGroup:insert( heartLeft )
		gameGroup:insert( heartLeft2 )
		gameGroup:insert( heartLeft3 )
		gameGroup:insert( heartLeft4 )
		gameGroup:insert( heartRight )
		gameGroup:insert( bestScoreText )
		--gameGroup:insert ( highScoreStaticText )
	end
	
	-- ************************************************************** --
	
	--	gameLoop() -- Accelerometer Code for Player Movement
	
	-- ************************************************************** --
	
	local gameLoop = function( event )
		
		--if gameIsActive and frameCounter >= 2 then	--> uncomment for 60 fps
		if gameIsActive then
			menuIsActive = false
			
			moveTrees()
			--moveGrass()
			movegrasss()
			movePickups()
			moveCheckPoint()
			moveStar()
			-- moveBombs()
			moveBunnies()
			moveBadFence()
			moveMilk()
			
			-- Change high score text when high score is passed
			if newHighScore == false then
				local theScore = tonumber(getScore())
				local bestScore = tonumber(gameSettings["bestScore"])
				
				if theScore > bestScore then
					newHighScore = true
					dropNotification( "New High Score!" )

					bestScoreText:setTextColor( 225, 0, 0, 255)
				end
			end
			
			-- electroBubble
			if electroBubble.isVisible == true then
				electroBubble.x = playerObject.x
				electroBubble.y = playerObject.y
			end
			
			-- ***********************************************************
			
			-- END - Score-Based Achievements
			
			-- ***********************************************************
			-- ***********************************************************
			
			--frameCounter = 1		--> uncomment for 60 fps
			
			
			-- DOUBLE-HEART BASED ACHIEVEMENTS
			local gameScore = tonumber(getScore())
			
			if gameLives >= 2 and gameScore >= 90000 and ofAch[ "life_preserver" ] == false then
				ofAch[ "life_preserver" ] = true
			end
			
			if gameLives >= 2 and gameScore >= 70000 and ofAch[ "stingy_heart" ] == false then
				ofAch[ "stingy_heart" ] = true
			end
			
			if gameLives >= 2 and gameScore >= 50000 and ofAch[ "heart_saver" ] == false then
				ofAch[ "heart_saver" ] = true
			end
		end
		--[[
		else
			frameCounter = frameCounter + 1
		end
		]]--
		
		if menuIsActive == true then
			gameIsActive = false
			-- homeScreen()
			-- moveTrees()	
			-- moveGrass()
			-- movegrasss()
			-- moveStar()
			-- moveBombs()
			-- moveBadFence()
			-- moveMilk()
		end
	end
	
	-- END gameLoop()
	
	-- ************************************************************** --
	
	-- drawGameOverObjects() -- Draw things for the game over display
	
	-- ************************************************************** --
	
	local drawGameOverObjects = function()
		
		-- Draw banner for game title for quick start menu
		quickStartBanner = display.newImageRect( "cow-quest-title.png", 518, 320 )
		quickStartBanner.x = 710; quickStartBanner.y = 160
		quickStartBanner.isVisible = false

		-- ************************************************************** --
		
		-- home background

		homeBackGround = display.newImageRect("cow-quest-bg.png",630,320)
			
		homeBackGround.x = 215
		homeBackGround.y = 160 

		if display.pixelHeight == 1136 then
			homeBackGround.x = 290
		end

		-- ************************************************************** --
		
		-- dairy logo

		dairyLogo = display.newImageRect("dairy-logo.png",149,35)
			
		dairyLogo.x = 105
		dairyLogo.y = -40 
		dairyLogo.isVisible = false

		
		-- ************************************************************** --
		-- call our inserts
		-- ************************************************************** --

		gameGroup:insert( homeBackGround )
		gameGroup:insert( quickStartBanner )
		
		-- Setup "Play Now" Button
		local touchPlayNowBtn = function( event )
			analytics.logEvent("First Play Button Pressed")
			if event.phase == "release" and playNowBtn.isActive == true then
				
				playNowBtn.isActive = false
				
				-- Turn off gameLoop event listener just in case
				Runtime:removeEventListener( "enterFrame", gameLoop )
		
				-- Start the gameLoop again
				Runtime:addEventListener( "enterFrame", gameLoop )
				
				-- Play Sound
				local soundsOn = gameSettings[ "soundsOn" ]
				local musicOn = gameSettings[ "musicOn" ]
				
				if soundsOn == true then
					local freeChan = audio.findFreeChannel()
					audio.play( tapSound, { channel=freeChan } )
					
					-- running sound effect
					local freeChan2 = audio.findFreeChannel()
					audio.play( runningSound, { loops=-1, channel=freeChan2 } )
				end
				
				if musicOn then					
					audio.stop( )
					musicChan = audio.findFreeChannel()
					audio.setVolume( 0.5, { channel=musicChan } )
					
					if gameSettings[ "gameTheme" ] == "classic" then
						audio.play( gameMusic3, { loops=-1, channel=musicChan, fadein=4000  } )
						
					elseif gameSettings[ "gameTheme" ] == "spook" then
						audio.play( gameMusic2, { loops=-1, channel=musicChan, fadein=4000  } )
						
					end
				end
				
				recycleRound()
				
			end
		end
		
		playNowBtn = ui.newButton{
			defaultSrc = "start-quest.png",
			defaultX = 178,
			defaultY = 39,
			overSrc = "start-quest-o.png",
			overX = 178,
			overY = 39,
			onEvent = touchPlayNowBtn,
			id = "playNowButton",
			-- text = "",
			-- font = "Helvetica",
			-- textColor = { 255, 255, 255, 255 },
			-- size = 16,
			-- emboss = false
		}
		
		playNowBtn.xOrigin = 110; playNowBtn.yOrigin = 600
		playNowBtn.isVisible = false
		
		gameGroup:insert( playNowBtn )


		-- Setup Credits Button

		local touchCreditsBtn = function( event )
			analytics.logEvent("Credits Button Pressed")
			if event.phase == "release" and creditsBtn.isActive == true then
				helpBtn.isActive = false	--> prevent double-pushing of the button
				
				-- Play Sound
				local soundsOn = gameSettings[ "soundsOn" ]
				local musicOn = gameSettings[ "musicOn" ]
				
				if soundsOn == true then
					local freeChan = audio.findFreeChannel()
					audio.play( tapSound, { channel=freeChan } )
				end
				
				if musicOn == true then
					audio.stop( gameMusic1 )
				end

				local hideDairyLogo = function() 
					dairyLogo.isVisible = false; display.remove( dairyLogo ); dairyLogo = nil;
				end
				transition.to ( dairyLogo, { y=-100, time = 100, transition=easing.inOutExpo, onComplete=hideDairyLogo } )

				local hideCreditsBtn = function() 
					creditsBtn.isVisible = false; display.remove( creditsBtn ); creditsBtn = nil;
				end
				transition.to ( creditsBtn, { y=400, time = 100, transition=easing.inOutExpo, onComplete=hideCreditsBtn } )
				
				-- remove event listeners
				gameIsActive = false
				menuIsActive = false
				Runtime:removeEventListener( "enterFrame", gameLoop )
				Runtime:removeEventListener( "system", onSystem )
				
				-- main menu call
				director:changeScene( "creditsScreen" )
			end
		end

		creditsBtn = ui.newButton{
			defaultSrc = "credits.png",
			defaultX = 104,
			defaultY = 39,
			onEvent = touchCreditsBtn,
			id = "creditsButton",
			-- text = "",
			-- font = "Helvetica",
			-- textColor = { 255, 255, 255, 255 },
			-- size = 16,
			-- emboss = false
		}

		creditsBtn.xOrigin = 147; creditsBtn.yOrigin = 800
		creditsBtn.isVisible = true

		gameGroup:insert( creditsBtn )

		
		-- Setup "Help" Button
		local touchHelpBtn = function( event )
			analytics.logEvent("Help Button Pressed")
			if event.phase == "release" and helpBtn.isActive == true then
				helpBtn.isActive = false	--> prevent double-pushing of the button
				
				-- Play Sound
				local soundsOn = gameSettings[ "soundsOn" ]
				local musicOn = gameSettings[ "musicOn" ]
				
				if soundsOn == true then
					local freeChan = audio.findFreeChannel()
					audio.play( tapSound, { channel=freeChan } )
				end
				
				if musicOn == true then
					audio.stop( gameMusic1 )
				end

				local hideDairyLogo = function() 
					dairyLogo.isVisible = false; display.remove( dairyLogo ); dairyLogo = nil;
				end
				transition.to ( dairyLogo, { y=-100, time = 100, transition=easing.inOutExpo, onComplete=hideDairyLogo } )

				local hideCreditsBtn = function() 
					creditsBtn.isVisible = false; display.remove( creditsBtn ); creditsBtn = nil;
				end
				transition.to ( creditsBtn, { y=400, time = 100, transition=easing.inOutExpo, onComplete=hideCreditsBtn } )

				-- remove event listeners
				gameIsActive = false
				menuIsActive = false
				Runtime:removeEventListener( "enterFrame", gameLoop )
				Runtime:removeEventListener( "system", onSystem )
				
				-- main menu call
				director:changeScene( "helpScreen" )
			end
		end
		
		helpBtn = ui.newButton{
			defaultSrc = "helpbtn.png",
			defaultX = 69,
			defaultY = 39,
			onEvent = touchHelpBtn,
			id = "helpButton"
		}
		
		-- helpBtn:setReferencePoint( display.BottomRightReferencePoint )
		helpBtn.xOrigin = -300; helpBtn.yOrigin = 600
		helpBtn.isVisible = false
		
		gameGroup:insert( helpBtn )
		
		-- Draw two rectangles, one for above the display and one for below (so things look good on iPad)
		local topRect = display.newRect( 0, -160, 480, 160 )
		topRect:setFillColor( 0, 0, 0, 255 )
		
		local bottomRect = display.newRect( 0, 320, 480, 160 )
		bottomRect:setFillColor( 0, 0, 0, 255 )
		
		gameGroup:insert( topRect )
		gameGroup:insert( bottomRect )
	end
	
	-- ************************************************************** --
	
	--	showQuickStartMenu() -- When app is first opened
	
	-- ************************************************************** --
	
	local showQuickStartMenu = function()
		-- Pause all game movement (if it was on for some odd reason)
		if gameIsActive == true then gameIsActive = false; end
		menuIsActive = true
		
		--Runtime:removeEventListener( "accelerometer", onTilt )
		--Runtime:removeEventListener( "touch", touchPause )
		
		system.setIdleTimer( true ) -- turn on device sleeping
		
		-- Hide the player from the screen
		playerObject.isVisible = false
		playerObject.isBodyActive = false
		
		-- Make game move speed slow (for menu only)
		gameSettings["gameMoveSpeed"] = gameSettings["gameMoveSpeed"] * 0.4
		
		-- Hide some of the on-screen elements
		theScoreGroup.isVisible = false
		-- heartBackground.isVisible = false
		heartLeft.isVisible = false
		heartLeft2.isVisible = false
		heartLeft3.isVisible = false
		heartLeft4.isVisible = false
		heartRight.isVisible = false
		comboText.isVisible = false
		comboIcon.isVisible = false
		comboBackground.isVisible = false
		
		local loadQuickStartMenu = function()
		
			-- Fade in the game over shade
			--[[
			gameOverShade.isVisible = true
			gameOverShade.alpha = 0
			transition.to( gameOverShade, { time=500, alpha=1 } )
			]]--
			
			-- Slide the score banner from the right
			quickStartBanner.isVisible = true
			transition.to( quickStartBanner, { time=1000, x=260, transition=easing.inOutExpo } )

			if display.pixelHeight == 1136 then
				transition.to( quickStartBanner, { time=1000, x=295, transition=easing.inOutExpo } )
			end

			dairyLogo.isVisible = true
			transition.to( dairyLogo, { time=1000, y=280, transition=easing.inOutExpo } )

			if display.pixelHeight == 1136 then
				transition.to( dairyLogo, { time=1000, x=130, transition=easing.inOutExpo } )
			end

			
			-- Show "Play Now" Button
			playNowBtn.isVisible = true
			transition.to( playNowBtn, { time=2500, y=190, transition=easing.inOutExpo } )

			if display.pixelHeight == 1136 then
				transition.to( playNowBtn, { time=1000, x=140, transition=easing.inOutExpo } )
			end

			-- Show "Credits" Button
			creditsBtn.isVisible = true
			transition.to( creditsBtn, { time=2500, y=235, transition=easing.inOutExpo } )

			if display.pixelHeight == 1136 then
				transition.to( creditsBtn, { time=2500, x=177, transition=easing.inOutExpo } )
			end
			
			-- Show "Help" Button
			helpBtn.isVisible = true
			-- helpBtn:setReferencePoint( display.BottomRightReferencePoint )
			helpBtn.x = 55
			transition.to( helpBtn, { time=3000, y=235, transition=easing.inOutExpo } )

			if display.pixelHeight == 1136 then
				transition.to( helpBtn, { time=1000, x=85, transition=easing.inOutExpo } )
			end
			
			-- Show "Tournament" Button
			--[[
			tournamentBtn.isVisible = true
			tournamentBtn:setReferencePoint( display.BottomLeftReferencePoint )
			tournamentBtn.x = 0
			transition.to( tournamentBtn, { time=1000, y=320, transition=easing.inOutExpo } )
			]]--
		end
		
		loadQuickStartMenu()
		
	end
	
	-- ************************************************************** --

	-- gameInit() --> set initial settings and call creation functions
	
	-- ************************************************************** --
	local gameInit = function()
		local musicOn = gameSettings[ "musicOn" ]
		local soundsOn = gameSettings[ "soundsOn" ]
		
		-- load difficulty from settings file
		local difficultySetting = loadValue( "difficulty.data" )
		
		if difficultySetting == "0" then
			-- Medium
			gameSettings[ "difficulty" ] = "medium"
			gameSettings["gameMoveSpeed"] = 7.7
			gameSettings["defaultMoveSpeed"] = 7.7
			
			print ( "Difficulty: Medium" )
		
		elseif difficultySetting == "1" then
			-- Easy
			gameSettings[ "difficulty" ] = "easy"
			gameSettings["gameMoveSpeed"] = 7.3
			gameSettings["defaultMoveSpeed"] = 7.3
			
			print ( "Difficulty: Easy" )
		
		elseif difficultySetting == "2" then
			-- Hard
			gameSettings[ "difficulty" ] = "hard"
			gameSettings["gameMoveSpeed"] = 8.1
			gameSettings["defaultMoveSpeed"] = 8.1
			
			print ( "Difficulty: Hard" )
		end
		
		-- load best score, lifetime grasss, highest combo, and unlocked items
		
		if gameSettings[ "difficulty" ] ~= "easy" then
			gameSettings[ "bestScore" ] = loadValue( "TiMQGcpCZv.data" )	
			gameSettings[ "lifegrasss" ] = loadValue( "SadzCtDWmK.data" )
			gameSettings[ "highCombo" ] = loadValue( "UVIMSPUuCb.data" )
		else
			gameSettings[ "bestScore" ] = loadValue( "TpjixLATIZ.data" )	
			gameSettings[ "lifegrasss" ] = loadValue( "sOfvDxAlkH.data" )
			gameSettings[ "highCombo" ] = loadValue( "wnpzK3g55u.data" )
		end
		
		gameSettings[ "tiltSpeed" ] = loadValue( "tilt.data" )
		
		local musicData = loadValue( "music.data" )
		local soundData = loadValue( "sound.data" )
		local themeData = loadValue( "theme.data" )
		local charData = loadValue( "char.data" )
		
		gameSettings["shouldOptimize"] = false
		
		if gameSettings[ "tiltSpeed" ] == "0" then
			gameSettings[ "tiltSpeed" ] = "3"
			saveValue( "tilt.data", "3" )
		end
		
		if musicData == "0" then
			gameSettings["musicOn"] = false
			musicOn = false
			
			musicData = "no"
			saveValue( "music.data", musicData )
		else
			if musicData == "yes" then
				gameSettings["musicOn"] = true
				musicOn = true
			elseif musicData == "no" then
				gameSettings["musicOn"] = false
				musicOn = false
			end
		end
		
		if soundData == "0" then
			soundData = "yes"	--> default: yes
			saveValue( "sound.data", soundData )
			
			gameSettings["soundsOn"] = true	--> default: true
			soundsOn = true		--> default: true
		elseif soundData == "yes" then
			
			gameSettings["soundsOn"] = true
			soundsOn = true
		
		elseif soundData == "no" then
			
			gameSettings["soundsOn"] = false
			soundsOn = false
		
		end
		
		-- set up proper theme based on saved file
		
		if themeData == "0" then
			themeData = "classic"
			saveValue( "theme.data", themeData )
			gameSettings["gameTheme"] = themeData
		
		else
			gameSettings["gameTheme"] = themeData
		end
		
		-- set up proper character based on saved file
		
		if charData == "0" then
			charData = "d"
			saveValue( "char.data", charData )
			gameSettings["gameChar"] = charData
		else
			gameSettings["gameChar"] = charData
		end
			
		
		-- start physics and set initial settings
		physics.start( true )
		physics.setGravity( 0, 0 )
		
		if musicOn then			
			musicChan = audio.findFreeChannel()
			
			audio.setVolume( 0.5, { channel=musicChan } )
			audio.play( gameMusic1, { loops=-1, channel=musicChan } )
		end
		
		
		-- Initial settings
		grassCombo = 0
		
		-- Populate random tables
		for i = 1, maxBunnyIndice do
			randomBunnyLocations[i] = mRandom( 1, 479 )
		end
		
		for i = 1, maxgrassIndice do
			randomgrassLocations[i] = mRandom( 70, 410 )
		end
		
		for i = 1, maxOneFourIndice do
			random1to4Table[i] = mRandom( 1, 4 )
		end
		
		drawBackground()
		-- drawFence()
		--createGrass()
		drawHighScore()
		createCheckPoint()
		createmilk()
		creategrasss()
		createStar()
		createPickups()
		-- createBombs()
		createPlayer()
		createBadFence()
		createBunnies()
		creatFence()
		
		drawHUD()
		drawGameOverObjects()
		
		Runtime:addEventListener( "enterFrame", gameLoop )
		Runtime:addEventListener( "system", onSystem )
		
		-- Set the game in motion
		showQuickStartMenu()
	end
	
	local onOrientationChange = function( event )
		-- update variable for orientation changes
		orientationDirection = event.type
	end
	 
	Runtime:addEventListener( "orientation", onOrientationChange )
	
	--***************************************************

	-- clean() --> stop timers and event listeners
	
	--***************************************************
	
	clean = function()
		gameIsActive = false
		menuIsActive = false
		physics.stop()
		
		-- stop event listeners
		Runtime:removeEventListener( "accelerometer", onTilt )
		Runtime:removeEventListener( "enterFrame", gameLoop )
		Runtime:removeEventListener( "touch", touchPause )
		Runtime:removeEventListener( "system", onSystem )
		Runtime:removeEventListener( "orientation", onOrientationChange )
		
		-- stop timers
		if playerObject.animTimer then
			timer.cancel( playerObject.animTimer )
		end
		
		if flyingBunny1.animTimer then
			timer.cancel( flyingBunny1.animTimer )
		end

		if flyingBunny2.animTimer then
			timer.cancel( flyingBunny2.animTimer )
		end
		
		if flyingBunny3 then
			if flyingBunny3.animTimer then
				timer.cancel( flyingBunny3.animTimer )
			end
		end
		
		-- unload all sounds and music
		unloadSoundsAndMusic()
	end
	
	
	-- LOAD THIS MODULE AND START A NEW ROUND:
	gameInit()
	
	-- MUST return a display.newGroup()
	return gameGroup
end
