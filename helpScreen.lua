-- 
-- Abstract: Tilt Monster sample project 
-- Designed and created by Jonathan and Biffy Beebe of Beebe Games exclusively for Ansca, Inc.
-- http://jonbeebe.net/
-- 
-- Version: 2.0.1
-- 
-- Sample code is MIT licensed, see http://developer.anscamobile.com/code/license
-- Copyright (C) 2010 ANSCA Inc. All Rights Reserved.

module(..., package.seeall)

-- Main function - MUST return a display.newGroup()
function new()
	local helpGroup = display.newGroup()
	
	local ui = require("ui")
	
	-- OPTIONS
	--local soundsOn = true; local soundsData
	
	-- OBJECTS
	local menuBtn
	
	--***************************************************

	-- drawBackground()
	
	--***************************************************
	
	local drawBackground = function()
		local helpBackground = display.newImageRect( "helpScreen.png", 640, 320 )
		helpBackground.x = 240; helpBackground.y = 160

		if display.pixelHeight == 1136 then
			helpBackground.x=280
		end
		
		helpGroup:insert( helpBackground )
	end

	-- drawTitle()
	
	--***************************************************
	
	local drawTitle = function()
		local headTitle = display.newImageRect( "herd-title.png", 407, 60 )
		headTitle.x = 240; headTitle.y = 50

		if display.pixelHeight == 1136 then
			headTitle.x=280
		end
		
		helpGroup:insert( headTitle )
	end
	
	--***************************************************

	-- drawButtons()
	
	--***************************************************
	
	local drawButtons = function()
		-- Setup "Menu" Button
		local touchMenuBtn = function( event )
			if event.phase == "release" then
				
				-- close the web popup
				native.cancelWebPopup()
				
				-- main menu call
				director:changeScene( "gotomainmenu" )
			end
		end
		
		menuBtn = ui.newButton{
			defaultSrc = "main-menu.png",
			defaultX = 130,
			defaultY = 28,
			overSrc = "main-menu-o.png",
			overX = 130,
			overY = 28,
			onEvent = touchMenuBtn,
			id = "menuButton"
		}
		
		menuBtn:setReferencePoint( display.BottomLeftReferencePoint )
		menuBtn.xOrigin = 20; menuBtn.yOrigin = 450
		menuBtn.isVisible = false
		
		helpGroup:insert( menuBtn )
		
		-- Show "Menu" Button
		menuBtn.isVisible = true
		menuBtn:setReferencePoint( display.BottomLeftReferencePoint )
		menuBtn.x = 20

		if display.pixelHeight == 1136 then
			menuBtn.x=55
		end

		transition.to( menuBtn, { time=1500, y=260, transition=easing.inOutExpo } )
		
	end

	--***************************************************

	-- drawType()
	
	--***************************************************
	
	local drawType = function()
		local helpType = display.newImageRect( "help-type.png", 470, 141 )
		helpType.x = 240; helpType.y = 190

		if display.pixelHeight == 1136 then
			helpType.x=280
		end
		
		helpGroup:insert( helpType )
	end
	
	--***************************************************

	-- showHelpPopup()
	
	--***************************************************
	
	-- local showHelpPopup = function()
	-- 	local topLoc = 73
		
	-- 	if system.getInfo("model") == "iPad" then
	-- 		topLoc = 73 + 34
	-- 	end
		
	-- 	native.showWebPopup( 15, topLoc, 440, 253, "help.html", {baseUrl=system.ResourceDirectory, hasBackground=false } ) 
		
	-- end
	
	--***************************************************

	-- init()
	
	--***************************************************
	
	local init = function()
		
		drawBackground()
		drawType()
		drawButtons()
		drawTitle()
		
		-- showHelpPopup()	--> display local help.html file
		
	end
		
	init()
	
	-- MUST return a display.newGroup()
	return helpGroup
end
