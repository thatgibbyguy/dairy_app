-- For "tall" sizes (iPhone 5 and new iTouch)
if display.pixelHeight > 960 then
 
        application = {
                content = {
                        width = 320,
                        height = 568, 
                        scale = "letterbox",
                        fps = 30,
 
                        imageSuffix = {
                            ["@2x"] = 2,
                        }
                }
        }
 
else -- For traditional sizes (iPhone 4S & below, old iTouch)
 
        application = {
                content = {
                        width = 320,
                        height = 480, 
                        scale = "letterbox",
                        fps = 30,
 
                        imageSuffix = {
                            ["@2x"] = 2,
                        }
                }
        }
                
end